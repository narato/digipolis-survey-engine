﻿cd $env:USERPROFILE\Source\Repos\narato-common\Narato.Common\tests\Narato.Common.Test
&"$env:USERPROFILE\.nuget\packages\OpenCover\4.6.519\tools\OpenCover.Console.exe" "-target:${env:programfiles}\dotnet\dotnet.exe" "-targetargs: test" "-register:User" "-filter:+[Narato.Common*]* -[*.Test]*" "-output:Unit_Test_Coverage_Report.xml"


cd $env:USERPROFILE\Source\Repos\digipolis-common\Digipolis.Common\tests\Digipolis.Common.Test
&"$env:USERPROFILE\.nuget\packages\OpenCover\4.6.519\tools\OpenCover.Console.exe" "-target:${env:programfiles}\dotnet\dotnet.exe" "-targetargs: test" "-register:User" "-filter:+[Digipolis.Common*]* -[*.Test]* -[Digipolis.Common]Digipolis.Common.Status.Models* -[Digipolis.Common]Digipolis.Common.Tenancy*" "-output:Unit_Test_Coverage_Report.xml"


cd $env:USERPROFILE\Source\Repos\digipolis-tenant-library\Digipolis.Tenant.Library\tests\Digipolis.Tenant.Library.Test
&"$env:USERPROFILE\.nuget\packages\OpenCover\4.6.519\tools\OpenCover.Console.exe" "-target:${env:programfiles}\dotnet\dotnet.exe" "-targetargs: test" "-register:User" "-filter:+[Digipolis.Tenant.Library*]* -[*.Test]* -[Digipolis.Tenant.Library]Digipolis.Tenant.Library.Migrations* -[Digipolis.Tenant.Library]Digipolis.Tenant.Library.Program" "-output:Unit_Test_Coverage_Report.xml"


cd $env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.FormEngine.Test
&"$env:USERPROFILE\.nuget\packages\OpenCover\4.6.519\tools\OpenCover.Console.exe" "-target:${env:programfiles}\dotnet\dotnet.exe" "-targetargs: test" "-register:User" "-filter:+[Digipolis.FormEngine.*]* -[*.Test]* -[Digipolis.FormEngine.Model]Digipolis.FormEngine.Model.Program" "-output:Unit_Test_Coverage_Report.xml"

cd $env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.FormEngine.Common.Test
&"$env:USERPROFILE\.nuget\packages\OpenCover\4.6.519\tools\OpenCover.Console.exe" "-target:${env:programfiles}\dotnet\dotnet.exe" "-targetargs: test" -register:"User" "-filter:+[Digipolis.FormEngine.Common*]* -[*.Test]*" -output:"Unit_Test_Coverage_Report.xml"


&"$env:USERPROFILE\.nuget\packages\ReportGenerator\2.4.6-beta4\tools\ReportGenerator.exe" -reports:"$env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.FormEngine.Test\Unit_Test_Coverage_Report.xml;$env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.FormEngine.Common.Test\Unit_Test_Coverage_Report.xml;$env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.Common.Tenant.Library.Test\Unit_Test_Coverage_Report.xml;$env:USERPROFILE\Source\Repos\narato-common\Narato.Common\tests\Narato.Common.Test\Unit_Test_Coverage_Report.xml;$env:USERPROFILE\Source\Repos\digipolis-common\Digipolis.Common\tests\Digipolis.Common.Test\Unit_Test_Coverage_Report.xml;$env:USERPROFILE\Source\Repos\digipolis-tenant-library\Digipolis.Tenant.Library\tests\Digipolis.Tenant.Library.Test\Unit_Test_Coverage_Report.xml" -targetdir:"$env:USERPROFILE\Source\Repos\digipolis-form-engine\OverallTestReport\UnitTestCoverage_Report"


cd $env:USERPROFILE\Source\Repos\narato-common\Narato.Common\