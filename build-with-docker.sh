#!/bin/bash

# If we keep this script in the project repo's root
# and set the working-dir from here,
# we can decouple our entire CI from product configuration
# aka as less bamboo settings as needed
cd Digipolis.FormEngine

# Get the full path to the directory where this script is stored
FULL_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $FULL_PATH

chmod 777 ci-entrypoint.sh
chmod 777 run-test-suite.sh
chmod 777 build-admin-api.sh
chmod 777 build-response-api.sh
chmod 777 build-template-api.sh

echo "moving local packages, so they can actually be used in the dotnet restore phase"
mv ../local_packages local_packages

pwd
ls local_packages

docker build -f Dockerfile \
  -t digipolisacpaas/formengine-ci:latest .

# execute restore & build
docker run --rm --privileged=true \
  -v $FULL_PATH/:/app \
  --rm -i digipolisacpaas/formengine-ci:latest

# Create gzip tarball of entire solution
# TODO: do this per project
tar -cvzf ../artifact.tar.gz .

# cleanup images
docker images \
  | awk '{ print $1,$2,$3 }' \
  | grep digipolisacpaas/formengine-ci \
  | awk '{ print $3 }' \
  | xargs -I {} docker rmi {}
