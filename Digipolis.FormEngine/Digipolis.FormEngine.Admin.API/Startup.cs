﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Digipolis.FormEngine.Domain.Mappers;
using Digipolis.FormEngine.Model;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Reflection;
using Digipolis.FormEngine.DataProvider.DbContexts;
using Digipolis.FormEngine.Admin.API.ActionExecutors;
using Digipolis.FormEngine.Common.Models;
using System.Net.Http;
using Digipolis.FormEngine.Admin.Api.Models.Validator;
using Narato.Common.ActionFilters;
using Narato.Common.Exceptions;
using Narato.Common.Factory;
using Digipolis.Tenant.Library.Managers;
using Digipolis.Tenant.Library.DataProviders;
using Narato.Common.Interfaces;
using Digipolis.Tenant.Library.ActionExecutors;
using Digipolis.Common.Tenancy.Mappers;
using Digipolis.Tenant.Library.Models;
using Digipolis.Common.Events;
using Digipolis.Tenant.Library.Tenancy;
using Digipolis.Common.Events.Config;
using Digipolis.Common.Status.Interfaces;
using Narato.Common.Checksum;
using Digipolis.Tenant.Library.QueryProviders;
using Digipolis.FormEngine.Common;
using Swashbuckle.Swagger.Model;
using NLog.Extensions.Logging;
using Digipolis.Common.Status.Models;
using Narato.Common.Mappers;

namespace Digipolis.FormEngine.Admin.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("config.json");

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.Configure<EngineConfiguration>(Configuration.GetSection("EngineConfiguration"));
            services.Configure<SystemConfiguration>(Configuration.GetSection("SystemConfiguration"));
            services.Configure<EventHandlerConfiguration>(Configuration.GetSection("EventHandlerConfiguration"));
            services.Configure<TenantConfiguration>(Configuration.GetSection("TenantConfiguration"));

            services.AddCors();

            services.AddMvc(
            //Add this filter globally so every request runs this filter to recored execution time
            config =>
            {
                config.Filters.Add(new ExecutionTimingFilter());
            })
                //Add formatter for JSON output to client and to format received objects         
                .AddJsonOptions(x =>
                {
                    x.SerializerSettings.ContractResolver =
                     new CamelCasePropertyNamesContractResolver();
                });


            var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);


            //Swagger configuation + swagger UI
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Contact = new Contact { Name = "Narato NV" },
                    Description = "Admin API",
                    Version = "v1",
                    Title = "Form & Survey Engine"
                });
                //TODO: Find another way to get access to this file, CopyToOutput in project.json crashes when running docker
                options.IncludeXmlComments($"{path}/Digipolis.Tenant.Library.xml");
                options.OperationFilter<ProducesConsumesFilter>();
            });


            //Gets the Admin Api Endpoint from the config.json file
            var engineConfigSection = Configuration.GetSection("EngineConfiguration");
            var connectionString = engineConfigSection.GetValue<string>("ConnectionString");
            var useEventHandler = engineConfigSection.GetValue<bool>("UseEventhandler");

            var eventHandlerConfiguration = Configuration.GetSection("EventHandlerConfiguration");
            var eventHandlerEndpoint = eventHandlerConfiguration.GetValue<string>("EventHandlerEndpoint");


            //Confgure Autofac
            var builder = new ContainerBuilder();

            //TODO: Make base interface so this configuration can be simplified

            builder.RegisterType<ExceptionHandler>().As<IExceptionHandler>();
            builder.RegisterType<ExceptionToActionResultMapper>().As<IExceptionToActionResultMapper>();
            builder.RegisterType<ChecksumCalculator>().As<ChecksumCalculator>();
            builder.RegisterType<TenantCreator<Config>>().As<ITenantCreator<Config>>();
            builder.RegisterType<ResponseFactory>().As<IResponseFactory>();
            builder.RegisterType<TenantManager<Config>>().As<ITenantManager<Config>>();
            builder.RegisterType<TenantDataProvider>().As<ITenantDataProvider>();
            builder.RegisterType<AdminActionExecutor>().As<IActionExecutor<Config>>();
            builder.RegisterType<ConfigMapper<Config>>().As<IConfigMapper<Config>>();
            builder.RegisterType<MigrationQueryProvider>().As<IMigrationQueryProvider>();
            builder.RegisterType<QueryExecutor>().As<IQueryExecutor>();
            builder.RegisterType<MigrationManager>().As<IMigrationManager>();
            builder.RegisterType<SystemStatusAdminManager>().As<ISystemStatusManager>();
            builder.RegisterType<TenantDbContext>().As<BaseTenantDbContext>().WithParameter("connectionString", connectionString); 
            builder.RegisterType<ResponseDbContext>().As<ResponseDbContext>();
            builder.RegisterType<TemplateDbContext>().As<TemplateDbContext>();
            if (useEventHandler)
                builder.RegisterType<Digipolis.Common.Events.EventHandler>().As<IEventHandlerAdministrator>().WithParameter("httpClient", new HttpClient() { BaseAddress = new Uri(eventHandlerEndpoint) }).SingleInstance();
            else
                builder.RegisterType<Digipolis.Common.Events.EventHandler>().As<IEventHandlerAdministrator>().SingleInstance().WithParameter("httpClient", new HttpClient()).SingleInstance(); ;

            builder.RegisterType<TenantValidator<Config>>().As<ITenantValidator<Config>>();

            builder.Populate(services);
            var container = builder.Build();

            // Return the IServiceProvider resolved from the container.
            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            loggerFactory.AddNLog();

            env.ConfigureNLog("nlog.config");


            app.UseApplicationInsightsRequestTelemetry();

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUi();

            app.UseMvc();
        }
    }
}
