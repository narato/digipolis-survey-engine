﻿using Digipolis.FormEngine.Model;
using Digipolis.Tenant.Controllers;
using Digipolis.Tenant.Library.Managers;
using Digipolis.Tenant.Library.Tenancy;
using Narato.Common.Factory;

namespace Digipolis.FormEngine.Admin.API.Controllers
{
    public class TenantController : TenantController<Config>
    {
        public TenantController(ITenantManager<Config> tenantManager, IResponseFactory responseFactory, ITenantCreator<Config> tenantCreator) : base(tenantManager, tenantCreator, responseFactory)
        {
        }
    }
}
