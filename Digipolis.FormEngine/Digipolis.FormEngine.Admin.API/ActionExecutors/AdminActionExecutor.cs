﻿using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.DataProvider.DbContexts;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System.ComponentModel.DataAnnotations;
using Digipolis.FormEngine.Domain.Configuration;
using Digipolis.Tenant.Library.ActionExecutors;
using Digipolis.Common.Events;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Events.Models;
using Microsoft.Extensions.Options;
using Digipolis.FormEngine.Common.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Admin.API.ActionExecutors
{
    public class AdminActionExecutor : IActionExecutor<Config>
    {
        private readonly IEventHandlerAdministrator _eventHandlerAdministrator;
        private readonly IOptions<EngineConfiguration> _engineConfig;

        public AdminActionExecutor(IOptions<EngineConfiguration> engineConfig, IEventHandlerAdministrator eventHandlerAdministrator)
        {
            _eventHandlerAdministrator = eventHandlerAdministrator;
            _engineConfig = engineConfig;
        }

        public async Task OnTenantCreatedAsync(Tenant<Config> tenant)
        {
            var responseDbName = tenant.Config.ResponseDatabaseConfig;
            var templateDbName = tenant.Config.TemplateDatabaseConfig;

            using (var context = new ResponseDbContext())
            {
                context.DatabaseConfig = responseDbName as DatabaseConfig;
                // TODO: check whether below statement ensures that all implicit migrations are run! AKA test whether it runs additional migrations when the db was already created
                await context.Database.MigrateAsync();
            }

            using (var context = new TemplateDbContext())
            {
                context.DatabaseConfig = templateDbName as DatabaseConfig;
                await context.Database.MigrateAsync();
            }
        }

        public async Task OnTenantDeletedAsync(Tenant<Config> tenant)
        {
        }

        public async Task EnsureDbNamesAvailableAsync(Tenant<Config> tenant)
        {
            var responseDbName = tenant.Config.ResponseDatabaseConfig;
            var templateDbName = tenant.Config.TemplateDatabaseConfig;

            using (var context = new ResponseDbContext())
            {
                context.DatabaseConfig = responseDbName as DatabaseConfig;
                if (await DatabaseNameAlreadyExistsAsync(context))
                {
                    throw new ValidationException("Database already exists: " + context.DatabaseConfig.DatabaseName);
                }
            }

            using (var context = new TemplateDbContext())
            {
                context.DatabaseConfig = templateDbName as DatabaseConfig;
                if (await DatabaseNameAlreadyExistsAsync(context))
                {
                    throw new ValidationException("Database already exists: " + context.DatabaseConfig.DatabaseName);
                }
            }

            if (_engineConfig.Value.UseEventhandler)
            {
                // creating namespace and topic for eventhandler
                var namespaceConfig = new NamespaceConfiguration()
                {
                    Namespace = new Namespace()
                    {
                        Name = tenant.Config.EventNamespace,
                        Owner = tenant.Config.EventOwnerKey
                    }
                };
                _eventHandlerAdministrator.EnsureNamespaceExists(namespaceConfig);

                _eventHandlerAdministrator.EnsureTopicExists(tenant.Config.EventOwnerKey, tenant.Config.EventNamespace, Constants.RESPONSE_CREATED_TOPIC + tenant.Key);
                _eventHandlerAdministrator.EnsureTopicExists(tenant.Config.EventOwnerKey, tenant.Config.EventNamespace, Constants.RESPONSE_UPDATED_TOPIC + tenant.Key);
            }
        }

        private async Task<bool> DatabaseNameAlreadyExistsAsync(BaseDbContext context)
        {
            return await (context.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).ExistsAsync();
        }

        public async Task BeforeTenantCreateAsync(Tenant<Config> tenant)
        {
            await EnsureDbNamesAvailableAsync(tenant);
        }
    }
}
