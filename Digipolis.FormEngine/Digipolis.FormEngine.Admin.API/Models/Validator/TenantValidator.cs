﻿using System.Collections.Generic;
using Digipolis.FormEngine.Model;
using Digipolis.Tenant.Library.Tenancy;
using Digipolis.Common.Tenancy.MappedModels;
using Narato.Common.Models;
using Narato.Common.Exceptions;

namespace Digipolis.FormEngine.Admin.Api.Models.Validator
{
    public class TenantValidator<ConfigT> : ITenantValidator<Config>
    {
        public void ValidateForInsert(Tenant<Config> tenant)
        {
            List<FeedbackItem> feedbackItems = new List<FeedbackItem>();


            if (string.IsNullOrEmpty(tenant.Name))
                feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the name property.", Type = FeedbackType.ValidationError });

            if (tenant.Config == null)
                feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config property.", Type = FeedbackType.ValidationError });

            if (tenant.Config != null)
            {
                if (tenant.Config.ResponseDatabaseConfig == null)
                    feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - responseDatabaseConfig property.", Type = FeedbackType.ValidationError });

                if (tenant.Config.ResponseDatabaseConfig != null)
                {
                    if (string.IsNullOrEmpty(tenant.Config.ResponseDatabaseConfig.DatabaseName))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - responseDatabaseConfig - databaseName property.", Type = FeedbackType.ValidationError });

                    if (string.IsNullOrEmpty(tenant.Config.ResponseDatabaseConfig.Server))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - responseDatabaseConfig - server property.", Type = FeedbackType.ValidationError });

                    if (string.IsNullOrEmpty(tenant.Config.ResponseDatabaseConfig.Port))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - responseDatabaseConfig - port property.", Type = FeedbackType.ValidationError });

                    if (string.IsNullOrEmpty(tenant.Config.ResponseDatabaseConfig.User))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - responseDatabaseConfig - user property.", Type = FeedbackType.ValidationError });

                    if (string.IsNullOrEmpty(tenant.Config.ResponseDatabaseConfig.Password))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - responseDatabaseConfig - password property.", Type = FeedbackType.ValidationError });
                }

                if (tenant.Config.TemplateDatabaseConfig == null)
                    feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - templateDatabaseConfig property.", Type = FeedbackType.ValidationError });

                if (tenant.Config.TemplateDatabaseConfig != null)
                {
                    if (string.IsNullOrEmpty(tenant.Config.TemplateDatabaseConfig.DatabaseName))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - templateDatabaseConfig - databaseName property.", Type = FeedbackType.ValidationError });

                    if (string.IsNullOrEmpty(tenant.Config.TemplateDatabaseConfig.Server))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - templateDatabaseConfig - server property.", Type = FeedbackType.ValidationError });

                    if (string.IsNullOrEmpty(tenant.Config.TemplateDatabaseConfig.Port))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - templateDatabaseConfig - port property.", Type = FeedbackType.ValidationError });

                    if (string.IsNullOrEmpty(tenant.Config.TemplateDatabaseConfig.User))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - templateDatabaseConfig - user property.", Type = FeedbackType.ValidationError });

                    if (string.IsNullOrEmpty(tenant.Config.TemplateDatabaseConfig.Password))
                        feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - templateDatabaseConfig - password property.", Type = FeedbackType.ValidationError });
                }

                if (string.IsNullOrEmpty(tenant.Config.EventNamespace))
                    feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - eventNamespace property.", Type = FeedbackType.ValidationError });

                if (string.IsNullOrEmpty(tenant.Config.EventOwnerKey))
                    feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the config - eventOwnerKey property.", Type = FeedbackType.ValidationError });
            }

            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);
        }
    }
}
