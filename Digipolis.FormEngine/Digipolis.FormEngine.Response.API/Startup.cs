﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Digipolis.FormEngine.Domain.Managers;
using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.DataProvider.DataProviders;
using Digipolis.FormEngine.DataProvider.Interfaces;
using System.Reflection;
using Digipolis.FormEngine.Domain.Clients;
using Digipolis.FormEngine.Common.Models;
using System.Net.Http;
using System.IO;
using Digipolis.FormEngine.Model.Validator;
using Digipolis.FormEngine.Model.Validator.Interfaces;
using Narato.Common.ActionFilters;
using Narato.Common.Exceptions;
using Narato.Common.Interfaces;
using Narato.Common.Factory;
using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Events;
using Digipolis.Common.Events.Config;
using Microsoft.AspNetCore.Http;
using Digipolis.FormEngine.Domain.Providers;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.Common;
using Digipolis.FormEngine.Domain.AutomaticMigration;
using Swashbuckle.Swagger.Model;
using NLog.Extensions.Logging;
using Digipolis.FormEngine.Model.SwaggerSchemaFilter;
using Digipolis.Common.Status.Models;
using Narato.Common.Mappers;
using Digipolis.FormEngine.Domain.Converters;

namespace Digipolis.FormEngine.Response.API
{
    public class Startup
    {

        private IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("config.json");

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //get the engine configuration from the config.json
            services.Configure<EngineConfiguration>(Configuration.GetSection("EngineConfiguration"));
            services.Configure<SystemConfiguration>(Configuration.GetSection("SystemConfiguration"));
            services.Configure<EventHandlerConfiguration>(Configuration.GetSection("EventHandlerConfiguration"));

            services.AddCors();

            services.AddMvc(
            //Add this filter globally so every request runs this filter to recored execution time
            config =>
            {
                config.Filters.Add(new ExecutionTimingFilter());
            }) 
                //Add formatter for JSON output to client and to format received objects         
                .AddJsonOptions(x =>
                 {
                     x.SerializerSettings.ContractResolver =
                      new CamelCasePropertyNamesContractResolver();
                     x.SerializerSettings.Converters.Add(new JsonContentHolderConverter());
                 });


            var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            //Swagger configuation + swagger UI
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Contact = new Contact { Name = "Narato NV" },
                    Description = "Response API",
                    Version = "v1",
                    Title = "Form & Survey Engine"
                });
                options.IncludeXmlComments($"{path}/Digipolis.FormEngine.Response.API.xml");
                options.OperationFilter<ProducesConsumesFilter>();
                options.DocumentFilter<OmitIgnoredPropertiesDocumentfilter>();
                options.DocumentFilter<ChangeTypeOfJsonContentFilter>();
            });

            //Gets the Admin Api Endpoint from the config.json file
            var engineConfigSection = Configuration.GetSection("EngineConfiguration");
            var adminApiEndpoint = engineConfigSection.GetValue<string>("AdminApiEndpoint");
            var useEventHandler = engineConfigSection.GetValue<bool>("UseEventhandler");

            var eventHandlerConfiguration = Configuration.GetSection("EventHandlerConfiguration");
            var eventHandlerEndpoint = eventHandlerConfiguration.GetValue<string>("EventHandlerEndpoint");


            //Confgure Autofac
            var builder = new ContainerBuilder();

            builder.RegisterType<ExceptionHandler>().As<IExceptionHandler>();
            builder.RegisterType<ExceptionToActionResultMapper>().As<IExceptionToActionResultMapper>();
            builder.RegisterType<TenantClient>().As<ITenantClient>().WithParameter("httpClient", new HttpClient() { BaseAddress = new Uri(adminApiEndpoint) });
            builder.RegisterType<CurrentTenantProvider>().As<ICurrentTenantProvider<Config>>();
            builder.RegisterType<ResponseFactory>().As<IResponseFactory>();
                // TODO: TenantClient is also interfaced. rather than doing WithParameter here, register the TenantClient seperately and do WithParameter there for the HttpClient
            builder.RegisterType<ResponseManager>().As<IResponseManager>().WithParameter("tenantClient", new TenantClient(new HttpClient() {BaseAddress = new Uri(adminApiEndpoint) }));
            builder.RegisterType<SystemStatusResponseManager>().As<ISystemStatusManager>().WithParameter("tenantClient", new TenantClient(new HttpClient() { BaseAddress = new Uri(adminApiEndpoint) }));
            builder.RegisterType<ResponseDbContextProvider>().As<IResponseDbContextProvider>();
            builder.RegisterType<ResponseDataProvider>().As<IResponseDataProvider>();
            builder.RegisterType<ResponseValidator>().As<IResponseValidator>();

            if (useEventHandler)
                builder.RegisterType<Digipolis.Common.Events.EventHandler>().As<IEventHandlerAdministrator>().WithParameter("httpClient", new HttpClient() { BaseAddress = new Uri(eventHandlerEndpoint) }).SingleInstance();
            else
                builder.RegisterType<Digipolis.Common.Events.EventHandler>().As<IEventHandlerAdministrator>().WithParameter("httpClient", new HttpClient()).SingleInstance();
            builder.Register(c => (Digipolis.Common.Events.EventHandler)c.Resolve<IEventHandlerAdministrator>()).As<IEventPublisher>(); // basically an alias
            builder.Register(c => (Digipolis.Common.Events.EventHandler)c.Resolve<IEventHandlerAdministrator>()).As<IEventhandlerPing>(); // basically an alias


            builder.Populate(services);
            var container = builder.Build();

            // Return the IServiceProvider resolved from the container.
            return container.Resolve<IServiceProvider>();
        }

        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            loggerFactory.AddNLog();

            env.ConfigureNLog("nlog.config");

            app.UseApplicationInsightsRequestTelemetry();

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            app.UseMvc();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUi();

            var engineConfigSection = Configuration.GetSection("EngineConfiguration");
            var adminApiEndpoint = engineConfigSection.GetValue<string>("AdminApiEndpoint");

             new ResponseAutoMigration(new TenantClient(new HttpClient() { BaseAddress = new Uri(adminApiEndpoint) })).Migrate();
        }
    }
}
