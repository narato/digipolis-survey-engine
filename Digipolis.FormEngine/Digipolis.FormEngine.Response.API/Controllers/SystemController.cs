﻿using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Status.Models;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Response.API.Controllers
{
    [Route("api/[controller]")]
    public class SystemController : Controller
    {
        private ISystemStatusManager _systemStatusManager;
        private IResponseFactory _responseFactory;

        public SystemController(ISystemStatusManager systemStatusManager, IResponseFactory responseFactory)
        {
            _systemStatusManager = systemStatusManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// This method returns the status of the system for the given tenantkey
        /// </summary>
        /// <returns>A system status for the given tenantid</returns>
        // GET api/Response
        [ProducesResponseType(typeof(Response<SystemStatus>), (int)System.Net.HttpStatusCode.OK)]
        [HttpGet("status")]
        public async Task<IActionResult> GetAsync([FromHeader] string tenantKey)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));

            return await _responseFactory.CreateGetResponseAsync(async () => await _systemStatusManager.GetStatusAsync(), this.GetRequestUri());
        }
    }
}
