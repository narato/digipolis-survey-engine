﻿using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Model.RouteValues;
using Digipolis.FormEngine.Model.SwaggerResponseModels;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Response.API.Controllers
{
    [Route("api/[controller]")]
    public class ResponsesController : Controller
    {
        private IResponseManager _responseManager;
        private IResponseFactory _responseFactory;

        public ResponsesController(IResponseManager responseManager, IResponseFactory responseFactory)
        {
            _responseManager = responseManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// This method returns 1 response identified by its id for the given tenantid
        /// </summary>
        /// <param name="id">The id of the response to retrieve</param>
        /// <param name="tenantKey">The key value of the current tenant</param>
        /// <returns>The response identified by the id for the given tenantid</returns>
        // GET api/Response
        [ProducesResponseType(typeof(Response<Model.Response>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("{id}", Name = "GetResponseById")]
        public async Task<IActionResult> GetAsync([Required]Guid id, [FromHeader] string tenantKey)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));

            return await _responseFactory.CreateGetResponseAsync(async () => await _responseManager.GetResponseByIdAsync(id), this.GetRequestUri());
        }

        /// <summary>
        /// This method returns all responses that are made using the template identified by the given template and version
        /// </summary>
        /// <param name="templateLookupKey">The lookupkey of the template for which to retrieve all responses</param>
        /// <param name="templateVersion">The version of the template for which to retrieve all responses</param>
        /// <param name="tenantKey">The key value of the current tenant</param>
        /// <param name="page">The page for the pagination </param>
        /// <param name="pagesize">The page size for the pagination</param>
        /// <returns>The responses that are made using the template identified by the given template and version</returns>
        // GET api/Response
        [ProducesResponseType(typeof(Response<Model.Response>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("{templateLookupKey}/{templateVersion}")]
        public async Task<IActionResult> GetResponsesForTemplateAsync([Required]Guid templateLookupKey, string templateVersion, [FromHeader] string tenantKey, [FromQuery] int page = 1, [FromQuery] int pagesize = 10)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));

            return await _responseFactory.CreateGetResponseForCollectionAsync(async () => await _responseManager.GetResponseByTemplateAsync(templateLookupKey, templateVersion, page, pagesize), this.GetRequestUri());
        }

        /// <summary>
        /// This method saves 1 response given in the body of the request
        /// </summary>
        /// <param name="response">The response to be saved</param>
        /// /// <param name="tenantKey">The key value of the current tenant</param>
        /// <returns>The response that was saved</returns>
        // POST api/Response
        [ProducesResponseType(typeof(Response<Model.Response>), (int)System.Net.HttpStatusCode.Created)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody][Required]PostResponse response, [FromHeader] string tenantKey)
        {           
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.Header));
            if (response == null)
                missingParams.Add(new MissingParam("response", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            return await _responseFactory.CreatePostResponseAsync(async () => await _responseManager.InsertResponseAsync(response.CastToParentType()), this.GetRequestUri(), "GetResponseById", new PostResponseRouteValues { Controller = "Response", Id = response.Id}, new List<RouteValuesIdentifierPair> { new RouteValuesIdentifierPair {ModelIdentifier = "Id", RouteValuesIdentifier="Id" } });
        }

        /// <summary>
        /// This method updates 1 response given in the body of the request
        /// </summary>
        /// <param name="id">The id of the response to update</param>
        /// <param name="response">The response to be saved</param>
        /// <param name="tenantKey">The key value of the current tenant</param>
        /// <returns>The response that was updated</returns>
        // PUT api/Response
        [ProducesResponseType(typeof(Response<Model.Response>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync([Required]Guid id, [FromBody][Required]PutResponse response, [FromHeader] string tenantKey)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.Header));
            if (response == null)
                missingParams.Add(new MissingParam("response", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            return await _responseFactory.CreatePutResponseAsync(async () => await _responseManager.UpdateResponseAsync(response.CastToParentType()), this.GetRequestUri());
        }

        /// <summary>
        /// This method deletes 1 response identified by the key in the URI of the request
        /// </summary>
        /// <param name="id">The id of the response to delete</param>
        /// <param name="tenantKey">The key value of the current tenant</param>
        /// <returns>The response that was deleted</returns>
        // DELETE api/Response
        [ProducesResponseType(typeof(Response<Model.Response>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([Required]Guid id, [FromHeader] string tenantKey)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.Header));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            return await _responseFactory.CreatePutResponseAsync(async () => await _responseManager.DeleteResponseAsync(id), this.GetRequestUri());
        }
    }
}
