﻿
namespace Digipolis.FormEngine.Domain.Configuration
{
    public static class Constants
    {
        public const string RESPONSE_CREATED_TOPIC = "FormEngine.Responses.Created.";
        public const string RESPONSE_UPDATED_TOPIC = "FormEngine.Responses.Updtated.";
    }
}
