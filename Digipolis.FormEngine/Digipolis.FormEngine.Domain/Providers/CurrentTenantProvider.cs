﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Domain.Providers
{
    public class CurrentTenantProvider : ICurrentTenantProvider<Config>
    {
            private readonly IHttpContextAccessor _httpContextAccessor;
            private readonly ITenantClient _tenantClient;
            private readonly string _tenantKeyName;

            // cached tenant so that we don't need to fetch the tenant
            private Tenant<Config> cachedTenant;

            public CurrentTenantProvider(IHttpContextAccessor httpContextAccessor, ITenantClient tenantClient)
            {
                _httpContextAccessor = httpContextAccessor;
                _tenantClient = tenantClient;
                _tenantKeyName = "tenantKey";
            }

            private void AssertTenantKeyExists()
            {
            if (!_httpContextAccessor.HttpContext.Request.Headers.ContainsKey(_tenantKeyName))
                {
                    throw new Exception("No tenantKey found in headers.");
                }
            }

            // if force = true, the tenant will be fetched from the client even while it's already in the cache
            public async Task<Tenant<Config>> GetCurrentTenantAsync(bool force = false)
            {
                if (cachedTenant != null && !force)
                {
                    return cachedTenant;
                }

                AssertTenantKeyExists();

                // TODO : make that tenantKey string injectable if needed.
                cachedTenant = await _tenantClient.GetTenantAsync(_httpContextAccessor.HttpContext.Request.Headers[_tenantKeyName]);
                return cachedTenant;
            }

            public string GetCurrentTenantKey()
            {
                AssertTenantKeyExists();
                return _httpContextAccessor.HttpContext.Request.Headers[_tenantKeyName];
            }

            public Tenant<Config> GetCurrentTenant(bool force = false)
            {
                return GetCurrentTenantAsync(force).Result;
            }
        }
    }
