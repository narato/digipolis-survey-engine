﻿using Digipolis.FormEngine.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Dynamic;
using System.Linq;
using System.Reflection;

namespace Digipolis.FormEngine.Domain.Converters
{
    public class JsonContentHolderConverter : JsonConverter
    {
        public override bool CanWrite => true;
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return objectType.GetInterfaces().Contains(typeof(IJsonContentHolder));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);

            // for some bizar reason, JsonConvert.SerializeObject(jObject["content"], setting with CamelCaseContractResolver) doesn't actually do camel casing... so we have to do this dirty hack to fix it...
            var contentAsDynamic = jObject["content"].ToObject<ExpandoObject>();
            var serializedContent = JsonConvert.SerializeObject(contentAsDynamic, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            jObject["content"] = serializedContent;

            existingValue = Activator.CreateInstance(objectType);
            serializer.Populate(jObject.CreateReader(), existingValue);

            return existingValue;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var contentHolder = value as IJsonContentHolder;
            var jsonContent = JsonConvert.DeserializeObject<JObject>(contentHolder.Content);
            contentHolder.Content = null;

            // we need this, because if we use the serializer as is to serialize the object, we'll end up in an infinite loop
            var jsonTemplate = JObject.FromObject(contentHolder, new JsonSerializer { ContractResolver = serializer.ContractResolver });
            jsonTemplate["content"] = jsonContent;

            serializer.Serialize(writer, jsonTemplate);
        }
    }
}
