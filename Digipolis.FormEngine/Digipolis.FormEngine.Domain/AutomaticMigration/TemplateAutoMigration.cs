﻿using Digipolis.FormEngine.DataProvider.DbContexts;
using Digipolis.FormEngine.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using NLog;
using System;

namespace Digipolis.FormEngine.Domain.AutomaticMigration
{
    public class TemplateAutoMigration
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly ITenantClient _tenantClient;
        public TemplateAutoMigration(ITenantClient tenantClient)
        {
            _tenantClient = tenantClient;
        }

        public async void Migrate()
        {
            try
            {
                var tenants = await _tenantClient.GetTenantsAsync();
                
                foreach (var tenant in tenants)
                {
                    try
                    {
                        using (var templateDbContext = new TemplateDbContext())
                        {
                            templateDbContext.DatabaseConfig = tenant.Config.TemplateDatabaseConfig;
                            templateDbContext.Database.Migrate();
                        }
                    } catch (Exception ex)
                    {
                        Logger.Error(ex, $"Migration of the tenant: '{tenant.Name}, {tenant.Key}' has failed.");
                    }
                    
                }
            } catch(Exception ex)
            {
                Logger.Error(ex);
                //throw new Exception($"The automigration of the database for tenant '{currentTenant.Name}({currentTenant.Key})' was not succesfull: " + ex.Message, ex);
            }
        }
    }
}
