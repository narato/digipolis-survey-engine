﻿using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Domain.Interfaces
{
    public interface IResponseManager
    {
        Task<Model.Response> GetResponseByIdAsync(Guid id);
        Task<PagedCollectionResponse<IEnumerable<Model.Response>>> GetResponseByTemplateAsync(Guid templateLookupKey, string templateVersion, int page, int pagesize);
        Task<Model.Response> InsertResponseAsync(Model.Response response);
        Task<Model.Response> UpdateResponseAsync(Model.Response response);
        Task<Model.Response> DeleteResponseAsync(Guid id);
    }
}
