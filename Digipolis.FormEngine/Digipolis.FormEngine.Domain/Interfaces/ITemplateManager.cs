﻿using Digipolis.FormEngine.Model;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Domain.Interfaces
{
    public interface ITemplateManager
    {
        Task<Template> GetTemplateByLookupKeyAndVersionAsync(Guid lookupKey, string version);
        Task<PagedCollectionResponse<IEnumerable<Template>>> GetTemplatesForTenantAsync(int page, int pagesize);
        Task<Template> InsertTemplateAsync(Template template);
        Task<Template> UpdateTemplateAsync(Template template);
        Task<Template> DeleteTemplateAsync(Guid lookupKey, string version);
        IList<string> ValidateTemplate(Template template);
        Task<PagedCollectionResponse<IEnumerable<Template>>> GetTemplateByLookupKeyAndScopeAsync(Guid templateLookupKey, string[] scope, int page, int pagesize);
    }
}
