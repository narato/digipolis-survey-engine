﻿using System;
using System.Collections.Generic;

namespace Digipolis.FormEngine.Domain.Interfaces
{
    public interface IResponseClient
    {
        IEnumerable<Model.Response> GetResponsesForTemplate(Guid templateLookupKey, string templateVersion);
    }
}
