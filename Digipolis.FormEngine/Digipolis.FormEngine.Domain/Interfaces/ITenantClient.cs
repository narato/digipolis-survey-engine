﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.FormEngine.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Domain.Interfaces
{
    public interface ITenantClient
    {
        Task<Tenant<Config>> GetTenantAsync(string key);
        Task<List<Tenant<Config>>> GetTenantsAsync();
    }
}
