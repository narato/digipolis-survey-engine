﻿using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Model.Validator.Interfaces;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Digipolis.FormEngine.Model.Validator
{
    public class TemplateValidator : ITemplateValidator
    {
        private IResponseClient _responseClient;

        public TemplateValidator(IResponseClient responseClient)
        {
            _responseClient = responseClient;
        }

        public void ValidatrForInsert(Template template)
        {
            List<FeedbackItem> feedbackItems = new List<FeedbackItem>();

            if (string.IsNullOrEmpty(template.Name))
                feedbackItems.Add(new FeedbackItem() { Description = "The template has no value to the name property.", Type = FeedbackType.ValidationError });

            if (string.IsNullOrEmpty(template.Content))
                feedbackItems.Add(new FeedbackItem() { Description = "The template has no value for the content property.", Type = FeedbackType.ValidationError });

            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);
        }

        public void ValidateForUpdate(Template template)
        {
            List<FeedbackItem> feedbackItems = new List<FeedbackItem>();

            if (string.IsNullOrEmpty(template.Name))
                feedbackItems.Add(new FeedbackItem() { Description = "The template has no value to the name property.", Type = FeedbackType.ValidationError });

            if (string.IsNullOrEmpty(template.Content))
                feedbackItems.Add(new FeedbackItem() { Description = "The template has no value for the content property.", Type = FeedbackType.ValidationError });

            if (template.LookupKey == null || template.LookupKey == Guid.Empty)
                feedbackItems.Add(new FeedbackItem() { Description = "The template has no value for the lookupkey property.", Type = FeedbackType.ValidationError });

            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);
        }

        public void ValidateForDelete(Guid lookupKey, string version)
        {
            List<FeedbackItem> feedbackItems = new List<FeedbackItem>();

            var responses = _responseClient.GetResponsesForTemplate(lookupKey, version);
            if (responses.Count() > 0)
                feedbackItems.Add(new FeedbackItem() { Description = "The template version to delete still has linked responses. Please remove the responses first before removing the template version.", Type = FeedbackType.ValidationError });

            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);
        }
    }
}
