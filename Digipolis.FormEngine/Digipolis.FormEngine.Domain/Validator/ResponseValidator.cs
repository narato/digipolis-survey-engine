﻿using Digipolis.FormEngine.Model.Validator.Interfaces;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using System;
using System.Collections.Generic;

namespace Digipolis.FormEngine.Model.Validator
{
    public class ResponseValidator : IResponseValidator
    {
        public void ValidateForInsert(Response response)
        {
            List<FeedbackItem> feedbackItems = new List<FeedbackItem>();

            if (Guid.Empty == response.TemplateLookupKey)
                feedbackItems.Add(new FeedbackItem() { Description = "The response has no value for the template lookup key property.", Type = FeedbackType.ValidationError });

            if (string.IsNullOrEmpty(response.TemplateVersion))
                feedbackItems.Add(new FeedbackItem() { Description = "The response has no value for the template version property.", Type = FeedbackType.ValidationError });

            if (string.IsNullOrEmpty(response.Content))
                feedbackItems.Add(new FeedbackItem() { Description = "The response has no value for the content property.", Type = FeedbackType.ValidationError });

            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);
        }

        public void ValidateForUpdate(Response response)
        {
            List<FeedbackItem> feedbackItems = new List<FeedbackItem>();

            if (Guid.Empty == response.Id)
                feedbackItems.Add(new FeedbackItem() { Description = "The response has no value for the id property.", Type = FeedbackType.ValidationError });

            if (Guid.Empty == response.TemplateLookupKey)
                feedbackItems.Add(new FeedbackItem() { Description = "The response has no value for the template lookup key property.", Type = FeedbackType.ValidationError });

            if (string.IsNullOrEmpty(response.TemplateVersion))
                feedbackItems.Add(new FeedbackItem() { Description = "The response has no value for the template version property.", Type = FeedbackType.ValidationError });

            if (string.IsNullOrEmpty(response.Content))
                feedbackItems.Add(new FeedbackItem() { Description = "The response has no value for the content property.", Type = FeedbackType.ValidationError });

            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);
        }
    }
}
