﻿using System;

namespace Digipolis.FormEngine.Model.Validator.Interfaces
{
    public interface ITemplateValidator
    {
        void ValidatrForInsert(Template template);
        void ValidateForUpdate(Template template);
        void ValidateForDelete(Guid lookupKey, string version);
    }
}
