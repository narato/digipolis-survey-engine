﻿namespace Digipolis.FormEngine.Model.Validator.Interfaces
{
    public interface IResponseValidator
    {
        void ValidateForInsert(Response response);
        void ValidateForUpdate(Response response);
    }
}
