﻿using Digipolis.Common.Events;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Common.Models;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Domain.Configuration;
using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.Model.Validator.Interfaces;
using Microsoft.Extensions.Options;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Domain.Managers
{
    public class ResponseManager : IResponseManager
    {
        private IResponseDataProvider _responseDataProvider;
        private readonly IEventPublisher _eventPublisher;
        private IResponseValidator _responseValidator;
        private ICurrentTenantProvider<Config> _currentDataProvider;
        private readonly IOptions<EngineConfiguration> _engineConfig;

        public ResponseManager(IOptions<EngineConfiguration> engineConfig, IResponseDataProvider responseDataProvider, IResponseValidator responseValidator, IEventPublisher eventPublisher, ICurrentTenantProvider<Config> currentTenantProvider)
        {
            _responseDataProvider = responseDataProvider;
            _responseValidator = responseValidator;
            _eventPublisher = eventPublisher;
            _currentDataProvider = currentTenantProvider;
            _engineConfig = engineConfig;
        }

        public async Task<Model.Response> GetResponseByIdAsync(Guid id)
        {
            return await _responseDataProvider.GetResponseByIdAsync(id);
        }

        public async Task<PagedCollectionResponse<IEnumerable<Model.Response>>> GetResponseByTemplateAsync(Guid templateLookupKey, string templateVersion, int page, int pagesize)
        {
            var count = await _responseDataProvider.GetCountByTemplateAsync(templateLookupKey, templateVersion);
            var data = await _responseDataProvider.GetResponseByTemplateAsync(templateLookupKey, templateVersion, page, pagesize);

            var response = new PagedCollectionResponse<IEnumerable<Model.Response>>();
            response.Data = data;
            response.Skip = (page - 1) * pagesize;
            response.Take = data.Count();
            response.Total = count;

            return response;
        }

        public async Task<Model.Response> InsertResponseAsync(Model.Response response)
        {
            _responseValidator.ValidateForInsert(response);

            response.Creation = DateTime.Now.ToUniversalTime();
            var createdResponse = await _responseDataProvider.InsertResponseAsync(response);

            if (_engineConfig.Value.UseEventhandler)
            {
                var tenant = await _currentDataProvider.GetCurrentTenantAsync();
                _eventPublisher.Publish(tenant.Config.EventOwnerKey, tenant.Config.EventNamespace, Constants.RESPONSE_CREATED_TOPIC + tenant.Key, new { id = createdResponse.Id });
            }

            return createdResponse;
        }

        public async Task<Model.Response> UpdateResponseAsync(Model.Response response)
        {
            _responseValidator.ValidateForUpdate(response);

            var updatedResponse = await _responseDataProvider.UpdateResponseAsync(response);

            if (_engineConfig.Value.UseEventhandler)
            {
                var tenant = await _currentDataProvider.GetCurrentTenantAsync();
                _eventPublisher.Publish(tenant.Config.EventOwnerKey, tenant.Config.EventNamespace, Constants.RESPONSE_UPDATED_TOPIC + tenant.Key, new { id = updatedResponse.Id });
            }

            return updatedResponse;
        }

        public async Task<Model.Response> DeleteResponseAsync(Guid id)
        { 
            return await _responseDataProvider.DeleteResponseAsync(id);
        }
    }
}
