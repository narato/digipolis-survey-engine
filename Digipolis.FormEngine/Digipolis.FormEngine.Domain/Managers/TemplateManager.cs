﻿using Digipolis.FormEngine.Domain.Interfaces;
using System;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Model.Validator.Interfaces;
using System.Collections.Generic;
using Narato.Common.Models;
using Narato.Common.Checksum;
using Narato.Common.Paging;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Linq;

namespace Digipolis.FormEngine.Domain.Managers
{
    public class TemplateManager : ITemplateManager
    {
        private ITemplateDataProvider _templateDataProvider;
        private ITenantClient _tenantClient;
        private ITemplateValidator _templateValidator;
        private ChecksumCalculator _checksumCalculator;
        private JSchema _jsonSchema;

        private const string SCOPE_DRAFT = "draft";
        private const string SCOPE_PUBLISHED = "published";
        private const string SCOPE_LATEST = "latest";

        public TemplateManager(ITemplateDataProvider templateDataProvider, ITemplateValidator templateValidator, ITenantClient tenantClient, ChecksumCalculator checksumCalculator, JSchema jsonSchema)
        {
            _templateDataProvider = templateDataProvider;
            _tenantClient = tenantClient;
            _templateValidator = templateValidator;
            _checksumCalculator = checksumCalculator;
            _jsonSchema = jsonSchema;
        }

        public async Task<Template> GetTemplateByLookupKeyAndVersionAsync(Guid lookupKey, string version)
        {
            return await _templateDataProvider.GetTemplateByLookupKeyAndVersionAsync(lookupKey, version);
        }

        public async Task<PagedCollectionResponse<IEnumerable<Template>>> GetTemplatesForTenantAsync(int page, int pagesize)
        {
            var count = await _templateDataProvider.GetCountAsync();
            var data = await _templateDataProvider.GetTemplatesAsync(page, pagesize);

            return data.ToPagedCollectionResponse(page, pagesize, count);
        }

        public async Task<PagedCollectionResponse<IEnumerable<Template>>> GetTemplateByLookupKeyAndScopeAsync(Guid templateLookupKey, string[] scope, int page, int pagesize)
        {
            var latestInScope = scope.Contains(SCOPE_LATEST, StringComparer.OrdinalIgnoreCase);
            var templateScope = DetermineScope(scope);

            if (latestInScope)
            {
                var data = await _templateDataProvider.GetLastInsertedTemplateScopedAsync(templateLookupKey, templateScope);
                if (data == null)
                {
                    return new List<Template>().ToPagedCollectionResponse(1, pagesize, 0);
                }
                var dataAsList = new List<Template>() { data }; // we need to return a list
                return dataAsList.ToPagedCollectionResponse(1, pagesize, 1);
            } else
            {
                var count = await _templateDataProvider.GetCountByLookupKeyScopedAsync(templateLookupKey, templateScope);
                var data = await _templateDataProvider.GetTemplatesByLookupKeyScopedAsync(templateLookupKey, templateScope, page, pagesize);
                return data.ToPagedCollectionResponse(page, pagesize, count);
            }
        }

        //TODO: move to own service
        private TemplateScope DetermineScope(string[] scope)
        {

            var draftsInScope = scope.Contains(SCOPE_DRAFT, StringComparer.OrdinalIgnoreCase);
            var publishedInScope = scope.Contains(SCOPE_PUBLISHED, StringComparer.OrdinalIgnoreCase);
            if (draftsInScope == publishedInScope)
            {
                return TemplateScope.EITHER;
            }
            return draftsInScope ? TemplateScope.DRAFT : TemplateScope.PUBLISHED;
        }

        public async Task<Template> InsertTemplateAsync(Template template)
        {
            _templateValidator.ValidatrForInsert(template);
            template.LookupKey = Guid.NewGuid();
            var omittedDraftProperty = template.IsDraft;
            template.IsDraft = true;
            template.Version = _checksumCalculator.CreateChecksum(template);
            template.IsDraft = omittedDraftProperty;
            template.Creation = DateTime.Now.ToUniversalTime();
            return await _templateDataProvider.InsertTemplateAsync(template);
        }

        public async Task<Template> UpdateTemplateAsync(Template template)
        {
            _templateValidator.ValidateForUpdate(template);

            var omittedDraftProperty = template.IsDraft;
            template.IsDraft = true;
            template.Version = _checksumCalculator.CreateChecksum(template);
            template.IsDraft = omittedDraftProperty;

            var existingTemplate = await GetTemplateByLookupKeyAndVersionAsync(template.LookupKey, template.Version);
            if (existingTemplate == null) // this version doesn't exist yet... we create a new one
            {
                template.Creation = DateTime.Now.ToUniversalTime();
                return await _templateDataProvider.InsertTemplateAsync(template);
            }
            existingTemplate.IsDraft = template.IsDraft;
            // now we update
            var updatedResponse = await _templateDataProvider.UpdateTemplateAsync(existingTemplate);

            return updatedResponse;
        }

        public async Task<Template> DeleteTemplateAsync(Guid lookupKey, string version)
        {
            _templateValidator.ValidateForDelete(lookupKey, version);

            return await _templateDataProvider.DeleteTemplateVersionAsync(lookupKey, version);
        }

        public IList<string> ValidateTemplate(Template template)
        {
            IList<string> output = new List<string>();
            try
            {
                JObject json = JsonConvert.DeserializeObject<JObject>(template.Content);
                json.IsValid(_jsonSchema, out output);
                return output;
            } catch (Exception)
            {
                output.Add("Given templateContent is not valid json.");
                return output;
            }
        }
    }
}
