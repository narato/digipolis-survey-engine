﻿using Digipolis.FormEngine.Common.Models;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Model;
using System;
using Microsoft.Extensions.Options;
using Digipolis.Common.Events;
using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Status.Models;
using Digipolis.Common.Tenancy.Providers;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Domain.Managers
{
    public class SystemStatusResponseManager : ISystemStatusManager
    {
        private IResponseDataProvider _responseDataProvider;
        private SystemConfiguration _systemConfig;
        private EngineConfiguration _engineConfig;
        private IEventhandlerPing _eventHandlerPing;
        private ICurrentTenantProvider<Config> _currentTenantProvider;

        public SystemStatusResponseManager(IOptions<SystemConfiguration> systemConfig, IOptions<EngineConfiguration> engineConfig, IResponseDataProvider responseDataProvider, IEventhandlerPing eventhandlerPing, ICurrentTenantProvider<Config> currentTenantProvider)
        {
            _responseDataProvider = responseDataProvider;
            _systemConfig = systemConfig.Value;
            _engineConfig = engineConfig.Value;
            _currentTenantProvider = currentTenantProvider;
            _eventHandlerPing = eventhandlerPing;
        }

        public SystemStatus GetStatus()
        {
            return GetStatusAsync().Result;
        }

        public async Task<SystemStatus> GetStatusAsync()
        {
            Tenant<Config> tenant = null;
            var systemStatus = new SystemStatus(Guid.NewGuid(), _systemConfig.Name, _systemConfig.Description, _systemConfig.Version, _systemConfig.Environment, _systemConfig.BuiltOn) { Up = true };

            var tenantKey = string.Empty;
            try {

                tenantKey = _currentTenantProvider.GetCurrentTenantKey();

                try
                {
                    tenant = await _currentTenantProvider.GetCurrentTenantAsync();
                    var dbCheck = await _responseDataProvider.CheckDBConnectionAsync();

                    if (!dbCheck.ConnectionOk)
                    {
                        systemStatus.Up = false;
                        systemStatus.Components.Add(
                            new Component()
                            {
                                Name = "Postgres Database",
                                Description = $"The response database for tenantkey '{tenantKey}'",
                                Up = false,
                                MoreInfo = "Connection to the Reponse database failed. Connection details:" +
                                $"Server:{tenant.Config.ResponseDatabaseConfig.Server}, " +
                                $"Port:{tenant.Config.ResponseDatabaseConfig.Port},  " +
                                $"Database:{tenant.Config.ResponseDatabaseConfig.DatabaseName}, " +
                                $"User:{tenant.Config.ResponseDatabaseConfig.User}. Reason: {dbCheck.Exception.Message}"
                            });
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    systemStatus.Up = false;
                    systemStatus.Components.Add(
                        new Component()
                        {
                            Name = "Form engine Admin api",
                            Description = $"The api used to retrieve the tenant config for tenantkey '{tenantKey}'",
                            Up = false,
                            MoreInfo = $"Retrieveing the tenant for tenantkey '{tenantKey}' failed with an unauthorized acces exception. Did you use the correct tenantkey?"
                        });
                }
                catch (Exception)
                {
                    systemStatus.Up = false;
                    systemStatus.Components.Add(
                        new Component()
                        {
                            Name = "Form engine Admin api",
                            Description = $"The api encountered an error trying to retrieve the tenant or the response database '{tenantKey}'",
                            Up = false
                        });
                }


            }
            catch (Exception)
            {
                systemStatus.Up = false;
                systemStatus.Components.Add(
                    new Component()
                    {
                        Name = "Form engine Admin api",
                        Description = $"The request is missing a header value for tenantKey",
                        Up = false
                    });
            }

            if (_engineConfig.UseEventhandler)
            {
                var eventHandlerCheckResult = false;
                string failMessage = string.Empty;
                try
                {
                    eventHandlerCheckResult = _eventHandlerPing.Ping();
                }
                catch (Exception ex) { failMessage = ex.Message; }


                if (!eventHandlerCheckResult)
                {
                    systemStatus.Up = false;
                    systemStatus.Components.Add(
                        new Component()
                        {
                            Name = "Eventhandler Engine",
                            Description = $"The connection to the eventhandler engine could not be made: " + failMessage,
                            Up = false
                        });
                }
            }
           

            return systemStatus;
        }
    }
}
