﻿using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Status.Models;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Model;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Domain.Managers
{
    public class SystemStatusTemplateManager : ISystemStatusManager
    {
        private ICurrentTenantProvider<Config> _currentTenantProvider;
        private SystemConfiguration _systemConfig;
        private readonly ITemplateDataProvider _templateDataProvider;

        public SystemStatusTemplateManager(IOptions<SystemConfiguration> systemConfig, ICurrentTenantProvider<Config> currentTenantProvider, ITemplateDataProvider templateDataProvider)
        {
            _currentTenantProvider = currentTenantProvider;
            _systemConfig = systemConfig.Value;
            _templateDataProvider = templateDataProvider;
        }

        public SystemStatus GetStatus()
        {
            return GetStatusAsync().Result;
        }

        public async Task<SystemStatus> GetStatusAsync()
        {
            Tenant<Config> tenant = null;
            var systemStatus = new SystemStatus(Guid.NewGuid(), _systemConfig.Name, _systemConfig.Description, _systemConfig.Version, _systemConfig.Environment, _systemConfig.BuiltOn) { Up = true };

            var tenantKey = string.Empty;
            try
            {
                tenantKey = _currentTenantProvider.GetCurrentTenantKey();

                try
                {
                    tenant = await _currentTenantProvider.GetCurrentTenantAsync();
                    var dbCheck = await _templateDataProvider.CheckDBConnectionAsync();

                    if (!dbCheck.ConnectionOk)
                    {
                        systemStatus.Up = false;
                        systemStatus.Components.Add(
                            new Component()
                            {
                                Name = "Postgres Database",
                                Description = $"The template database for tenantkey '{tenantKey}'",
                                Up = false,
                                MoreInfo = "Connection to the Template database failed. Connection details:" +
                                $"Server:{tenant.Config.TemplateDatabaseConfig.Server}, " +
                                $"Port:{tenant.Config.TemplateDatabaseConfig.Port},  " +
                                $"Database:{tenant.Config.TemplateDatabaseConfig.DatabaseName}, " +
                                $"User:{tenant.Config.TemplateDatabaseConfig.User}. Reason: {dbCheck.Exception.Message}"
                            });
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    systemStatus.Up = false;
                    systemStatus.Components.Add(
                        new Component()
                        {
                            Name = "Form engine Admin api",
                            Description = $"The api used to retrieve the tenant config for tenantkey '{tenantKey}'",
                            Up = false,
                            MoreInfo = $"Retrieveing the tenant for tenantkey '{tenantKey}' failed with an unauthorized acces exception. Did you use the correct tenantkey?"
                        });
                }
                catch (Exception)
                {
                    systemStatus.Up = false;
                    systemStatus.Components.Add(
                        new Component()
                        {
                            Name = "Form engine Admin api",
                            Description = $"The api encountered an error trying to retrieve the tenant or the template database '{tenantKey}'",
                            Up = false
                        });
                }


            }
            catch (Exception)
            {
                systemStatus.Up = false;
                systemStatus.Components.Add(
                    new Component()
                    {
                        Name = "Form engine Admin api",
                        Description = $"The request is missing a header value for tenantKey",
                        Up = false
                    });
            }

            return systemStatus;
        }
    }
}
