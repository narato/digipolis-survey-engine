﻿using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Model;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Digipolis.FormEngine.Domain.Clients
{
    public class ResponseClient : IResponseClient
    {
        #region Private members

        private readonly HttpClient _client;
        private readonly ICurrentTenantProvider<Config> _currentTenantProvider;

        #endregion Private members

        public ResponseClient(HttpClient httpClient, ICurrentTenantProvider<Config> currentTenantProvider)
        {
            _client = httpClient;
            if (_client.DefaultRequestHeaders.Accept.Count == 0)
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            _currentTenantProvider = currentTenantProvider;
        }

        public virtual IEnumerable<Model.Response> GetResponsesForTemplate(Guid templateLookupKey, string templateVersion)
        {
            var tenantkey = _currentTenantProvider.GetCurrentTenantKey();

            if (_client.DefaultRequestHeaders.Contains("tenantkey"))
                _client.DefaultRequestHeaders.Remove("tenantkey");

            _client.DefaultRequestHeaders.Add("tenantkey", tenantkey);

            var response = _client.GetAsync($"responses/{templateLookupKey}/{templateVersion}").Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var tenantResponse = JsonConvert.DeserializeObject<Response<IEnumerable<Model.Response>>>(responseString);
                return tenantResponse.Data;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var tenantResponse = JsonConvert.DeserializeObject<Response<IEnumerable<Model.Response>>>(responseString);
                throw new ExceptionWithFeedback(new FeedbackItem() { Description = $"The responses could not be retrieved. Received an internal server error from Response api.", Type = FeedbackType.Error });
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var tenantResponse = JsonConvert.DeserializeObject<Response<IEnumerable<Model.Response>>>(responseString);
                throw new ExceptionWithFeedback(new FeedbackItem() { Description = $"The responses could not be retrieved. Received an internal server error from Response api.", Type = FeedbackType.Error });
            }

            return null;
        }
    }
}
