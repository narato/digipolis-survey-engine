﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.Domain.Interfaces;
using System.Diagnostics;
using Digipolis.Common.Tenancy.MappedModels;
using Narato.Common.Models;
using Narato.Common.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Domain.Clients
{
    public class TenantClient : ITenantClient
    {
        #region Private members

        private readonly HttpClient _client;

        #endregion Private members

        public TenantClient(HttpClient httpClient)
        {
            _client = httpClient;
            if (_client.DefaultRequestHeaders.Accept.Count == 0)
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Tenant<Config>> GetTenantAsync(string key)
        {
            var response = await _client.GetAsync($"tenants/{key}");

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var tenantResponse = JsonConvert.DeserializeObject<Response<Tenant<Config>>>(responseString);
                Debug.WriteLine("Duration: " + tenantResponse.Generation.Duration);
                return tenantResponse.Data;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var tenantResponse = JsonConvert.DeserializeObject<Response<Tenant<Config>>>(responseString);
                throw new ExceptionWithFeedback(new FeedbackItem() { Description = $"The tenant could not be retrieved. Received an internal server error from Admin api.", Type = FeedbackType.Error });
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var tenantResponse = JsonConvert.DeserializeObject<Response<Tenant<Config>>>(responseString);
                throw new ExceptionWithFeedback(new FeedbackItem() { Description = $"The tenant could not be retrieved. Received an internal server error from Admin api.", Type = FeedbackType.Error });
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                // TODO: Create our own exception class for this. If you look at the description of the one we currently use,
                // it clearly states that it's supposed to be thrown on filesystem IO forbidden exceptions
                throw new UnauthorizedAccessException();
            }

            return null;
        }


        public async Task<List<Tenant<Config>>> GetTenantsAsync()
        {
            var response = await _client.GetAsync($"tenants");

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var tenantResponse = JsonConvert.DeserializeObject<Response<List<Tenant<Config>>>>(responseString);
                Debug.WriteLine("Duration: " + tenantResponse.Generation.Duration);
                return tenantResponse.Data;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var tenantResponse = JsonConvert.DeserializeObject<Response<List<Tenant<Config>>>>(responseString);
                throw new ExceptionWithFeedback(new FeedbackItem() { Description = $"The tenant could not be retrieved. Received an internal server error from Admin api.", Type = FeedbackType.Error });
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var tenantResponse = JsonConvert.DeserializeObject<Response<List<Tenant<Config>>>>(responseString);
                throw new ExceptionWithFeedback(new FeedbackItem() { Description = $"The tenant could not be retrieved. Received an internal server error from Admin api.", Type = FeedbackType.Error });
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                // TODO: Create our own exception class for this. If you look at the description of the one we currently use,
                // it clearly states that it's supposed to be thrown on filesystem IO forbidden exceptions
                throw new UnauthorizedAccessException();
            }

            return null;
        }
    }
}
