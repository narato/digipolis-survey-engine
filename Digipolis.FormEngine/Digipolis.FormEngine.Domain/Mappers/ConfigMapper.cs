﻿using Newtonsoft.Json;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Mappers;

namespace Digipolis.FormEngine.Domain.Mappers
{
    public class ConfigMapper<Config> : IConfigMapper<Config>
    {
        public Digipolis.Common.Tenancy.Models.Tenant Map(Tenant<Config> tenant)
        {
            return new Digipolis.Common.Tenancy.Models.Tenant()
            {
                Id = tenant.Id,
                Description = tenant.Description,
                Key = tenant.Key,
                Name = tenant.Name,
                Status = tenant.Status,
                Config = JsonConvert.SerializeObject(tenant.Config)
            };
        }

        public Tenant<Config> Map(Digipolis.Common.Tenancy.Models.Tenant tenant)
        {
            return new Tenant<Config>()
            {
                Id = tenant.Id,
                Description = tenant.Description,
                Key = tenant.Key,
                Name = tenant.Name,
                Status = tenant.Status,
                Config = JsonConvert.DeserializeObject<Config>(tenant.Config)
            };
        }
    }
}
