﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.ActionResult;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace Digipolis.FormEngine.Common.Test
{
    public class InternalServerErrorWithResponseTest
    {
        [Fact]
        public void Test()
        {
            var response = new Narato.Common.Models.Response();

            var isewr = new InternalServerErrorWithResponse(response);
            var headerDictionary = new HeaderDictionary();
            
            var httpResponse = new Mock<HttpResponse>() ;
            httpResponse.SetupGet(x => x.Headers).Returns(headerDictionary);
            httpResponse.SetupGet(x => x.Body).Returns(new MemoryStream());

            var httpContextMock = new Mock<HttpContext>();
            httpContextMock.SetupGet(x => x.Response).Returns(httpResponse.Object);

            var actionContext = new Mock<ActionContext>();
            actionContext.Object.HttpContext = httpContextMock.Object;
            

            var task = isewr.ExecuteResultAsync(actionContext.Object);

            Assert.Equal(task.Status, TaskStatus.RanToCompletion);

        }
    }
}
