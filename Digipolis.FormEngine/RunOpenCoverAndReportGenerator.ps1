﻿cd $env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.FormEngine.Test
&"$env:USERPROFILE\.nuget\packages\OpenCover\4.6.519\tools\OpenCover.Console.exe" "-target:${env:programfiles}\dotnet\dotnet.exe" "-targetargs: test" "-register:User" "-filter:+[Digipolis.FormEngine.*]* -[*.Test]* -[Digipolis.FormEngine.Model]Digipolis.FormEngine.Model.Program" "-output:Unit_Test_Coverage_Report.xml"

cd $env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.FormEngine.Common.Test
&"$env:USERPROFILE\.nuget\packages\OpenCover\4.6.519\tools\OpenCover.Console.exe" "-target:${env:programfiles}\dotnet\dotnet.exe" "-targetargs: test" -register:"User" "-filter:+[Digipolis.FormEngine.Common*]* -[*.Test]*" -output:"Unit_Test_Coverage_Report.xml"


&"$env:USERPROFILE\.nuget\packages\ReportGenerator\2.4.5\tools\ReportGenerator.exe" -reports:"$env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.FormEngine.Test\Unit_Test_Coverage_Report.xml;$env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.FormEngine.Common.Test\Unit_Test_Coverage_Report.xml;$env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\Digipolis.Common.Tenant.Library.Test\Unit_Test_Coverage_Report.xml" -targetdir:"$env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\UnitTestCoverage_Report"

cd $env:USERPROFILE\Source\Repos\digipolis-form-engine\Digipolis.FormEngine\