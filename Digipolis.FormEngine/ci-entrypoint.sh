#!/bin/bash

# Restore all packages in all projects
dotnet restore -f local_packages

# Build output projects
umask 000 && ./build-admin-api.sh
umask 000 && ./build-response-api.sh
umask 000 && ./build-template-api.sh

# Run test suites
umask 000 && ./run-test-suite.sh
