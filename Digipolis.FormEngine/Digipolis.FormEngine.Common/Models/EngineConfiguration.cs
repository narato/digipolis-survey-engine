﻿namespace Digipolis.FormEngine.Common.Models
{
    public class EngineConfiguration
    {
        public string AdminApiEndpoint { get; set; }
        public string ResponseApiEndpoint { get; set; }
        public bool UseEventhandler { get; set; }
    }
}
