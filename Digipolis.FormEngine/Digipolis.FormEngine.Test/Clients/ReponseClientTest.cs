﻿using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Domain.Clients;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.Test.Fakes;
using Moq;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Xunit;

namespace Digipolis.FormEngine.Test.Clients
{
    public class ReponseClientTest
    {
        [Fact]
        public void GetTemplatesReturnsTemplate()
        {
            var id = Guid.NewGuid();
            var lookupKey = Guid.NewGuid();

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.DefaultRequestHeaders.Add("tenantkey", "tenantkey");

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(new Response<List<Model.Response>>()
            {
                Data = new List<Model.Response> { new Model.Response { Id = id, Content = "Content", TemplateLookupKey = lookupKey, TemplateVersion = "Version" } }
            }));
            httpClient.BaseAddress = new Uri("http://test/api/");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
                    msg =>
                            msg.Method == HttpMethod.Get &&
                            msg.RequestUri.ToString() == $"http://test/api/responses/{lookupKey}/version")))
                            .Returns(httpResponseMessage);

            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(x => x.GetCurrentTenantKey()).Returns("tenantKey");

            var ResponseClient = new ResponseClient(httpClient, currentTenantProviderMock.Object);
            var responsesOut = ResponseClient.GetResponsesForTemplate(lookupKey, "version");


            Assert.NotNull(responsesOut);
            Assert.True(responsesOut.Count() == 1);
            Assert.Equal(id, responsesOut.ToList()[0].Id);
        }

        [Fact]
        public void ThrowsExceptionWithFeedbackOnBadRequestResultFromResponseAPI()
        {
            var id = Guid.NewGuid();
            var lookupKey = Guid.NewGuid();

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.DefaultRequestHeaders.Add("tenantkey", "tenantkey");

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(new Response<List<Model.Response>>()
            {
                Feedback= new List<FeedbackItem> { new FeedbackItem {Description="Feedback", Type=FeedbackType.Error } }
            }));
            httpClient.BaseAddress = new Uri("http://test/api/");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
                    msg =>
                            msg.Method == HttpMethod.Get &&
                            msg.RequestUri.ToString() == $"http://test/api/responses/{lookupKey}/version")))
                            .Returns(httpResponseMessage);

            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(x => x.GetCurrentTenantKey()).Returns("tenantKey");

            var responseClient = new ResponseClient(httpClient, currentTenantProviderMock.Object);

            ExceptionWithFeedback ex = Assert.Throws<ExceptionWithFeedback>(() => responseClient.GetResponsesForTemplate(lookupKey, "version"));
        }

        [Fact]
        public void ThrowsExceptionWithFeedbackOnInternalServerErrorResultFromResponseAPI()
        {
            var id = Guid.NewGuid();
            var lookupKey = Guid.NewGuid();

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.DefaultRequestHeaders.Add("tenantkey", "tenantkey");

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(new Response<List<Model.Response>>()
            {
                Feedback = new List<FeedbackItem> { new FeedbackItem { Description = "Feedback", Type = FeedbackType.Error } }
            }));
            httpClient.BaseAddress = new Uri("http://test/api/");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
                    msg =>
                            msg.Method == HttpMethod.Get &&
                            msg.RequestUri.ToString() == $"http://test/api/responses/{lookupKey}/version")))
                            .Returns(httpResponseMessage);

            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(x => x.GetCurrentTenantKey()).Returns("tenantKey");

            var responseClient = new ResponseClient(httpClient, currentTenantProviderMock.Object);

            ExceptionWithFeedback ex = Assert.Throws<ExceptionWithFeedback>(() => responseClient.GetResponsesForTemplate(lookupKey, "version"));
        }


    }
}
