﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.FormEngine.Domain.Clients;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.Test.Fakes;
using Moq;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Xunit;

namespace Digipolis.FormEngine.Test.Clients
{
    public class TenantClientTest
    {
        [Fact]
        public void GetTenantReturnsATenant()
        {
            var tenant = new Tenant<Config>();
            var key = "TenantKey";
            var id = Guid.NewGuid();
            tenant.Id = id;
            tenant.Key = key;
            tenant.Name = "TenantName";
            tenant.Status = "Status";
            tenant.Description = "Description";

            var config = new Config();
            config.ResponseDatabaseConfig = new DatabaseConfig() { DatabaseName = "ResponsedbName", Server = "server", Port = "5432", Password = "password", User = "user" };
            config.TemplateDatabaseConfig = new DatabaseConfig() { DatabaseName = "TemplatedbName", Server = "server", Port = "5432", Password = "password", User = "user" };

            tenant.Config = config;

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(new Response<Tenant<Config>>() { Data = tenant }));
            httpClient.BaseAddress = new Uri("http://test/api/");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
            msg =>
                    msg.Method == HttpMethod.Get &&
                    msg.RequestUri.ToString() == $"http://test/api/tenants/{key}")))
                    .Returns(httpResponseMessage);

            var tenantClient = new TenantClient(httpClient);
            var tenantOut = tenantClient.GetTenantAsync(key).Result;

            Assert.NotNull(tenantOut);
            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Id, id);
            Assert.Equal(tenantOut.Key, "TenantKey");
            Assert.Equal(tenantOut.Name, "TenantName");
            Assert.Equal(tenantOut.Status, "Status");
            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Config.ResponseDatabaseConfig.DatabaseName, "ResponsedbName");
            Assert.Equal(tenantOut.Config.TemplateDatabaseConfig.DatabaseName, "TemplatedbName");
        }

        [Fact]
        public void TenantClientThrowsExceptionWithFeedbackWhenTenantAPIReturnsBadRequest()
        {
            var key = "TenantKey";

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(new Response<Tenant<Config>>() { Feedback = new List<FeedbackItem> { new FeedbackItem() { Description = "feedback" } } }));
            httpClient.BaseAddress = new Uri("http://test/api/");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
            msg =>
                    msg.Method == HttpMethod.Get &&
                    msg.RequestUri.ToString() == $"http://test/api/tenants/{key}")))
                    .Returns(httpResponseMessage);

            var tenantClient = new TenantClient(httpClient);

            ExceptionWithFeedback ex = Assert.ThrowsAsync<ExceptionWithFeedback>(() => tenantClient.GetTenantAsync(key)).Result; 
        }

        [Fact]
        public void TenantClientThrowsUnauthorizedAccessExceptionWhenTenantAPIReturnsNotFound()
        {
            var key = "TenantKey";

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.NotFound);
            
            httpClient.BaseAddress = new Uri("http://test/api/");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
            msg =>
                    msg.Method == HttpMethod.Get &&
                    msg.RequestUri.ToString() == $"http://test/api/tenants/{key}")))
                    .Returns(httpResponseMessage);

            var tenantClient = new TenantClient(httpClient);

            UnauthorizedAccessException ex = Assert.ThrowsAsync<UnauthorizedAccessException>(() => tenantClient.GetTenantAsync(key)).Result;
        }

        [Fact]
        public void TenantClientReturnsNullWhenTentanAPIReturnsOtherStatusCode()
        {
            var key = "TenantKey";

            var msgHandler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.NoContent);

            httpClient.BaseAddress = new Uri("http://test/api/");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
            msg =>
                    msg.Method == HttpMethod.Get &&
                    msg.RequestUri.ToString() == $"http://test/api/tenants/{key}")))
                    .Returns(httpResponseMessage);

            var tenantClient = new TenantClient(httpClient);

            var empty = tenantClient.GetTenantAsync(key).Result;

            Assert.Null(empty);
        }
    }

   

    
}
