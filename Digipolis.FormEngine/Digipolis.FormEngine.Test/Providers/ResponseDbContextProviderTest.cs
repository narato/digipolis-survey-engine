﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Domain.Providers;
using Digipolis.FormEngine.Model;
using Moq;
using Xunit;

namespace Digipolis.FormEngine.Test.Providers
{
    public class ResponseDbContextProviderTest
    {
        //[Fact]
        public void CtorTest()
        {
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();

            var responsDbContextProvider = new ResponseDbContextProvider(currentTenantProviderMock.Object);

            Assert.NotNull(responsDbContextProvider);
        }

        //[Fact]
        public void ProvideReturnDbContext()
        {
            var tenant = new Tenant<Config>() { Config = new Config { ResponseDatabaseConfig = new DatabaseConfig() } };
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(tenant);

            var responsDbContextProvider = new ResponseDbContextProvider(currentTenantProviderMock.Object);

            var context = responsDbContextProvider.ProvideAsync().Result;
            currentTenantProviderMock.Verify(x => x.GetCurrentTenantAsync(false), Times.Once);
            Assert.Equal(tenant.Config.ResponseDatabaseConfig, context.DatabaseConfig);
        }
    }
}
