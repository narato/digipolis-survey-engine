﻿using Digipolis.FormEngine.DataProvider.DataProviders;
using Digipolis.FormEngine.DataProvider.DbContexts;
using Digipolis.FormEngine.Model;
using Microsoft.EntityFrameworkCore;
using Moq;
using Narato.Common.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Digipolis.FormEngine.Test.Providers
{
    public class TemplayeDataProviderTest
    {
        [Fact]
        public async Task InsertTemplateAddsTemplaTeToTemplates()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TemplateDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var templateDbContext = new TemplateDbContext(optionsBuilder.Options);

            var templateDbContextProviderMock = new Mock<ITemplateDbContextProvider>();
            templateDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(templateDbContext);

            var provider = new TemplateDataProvider(templateDbContextProviderMock.Object);

            var count = templateDbContext.Template.ToList().Count;
            var result = await provider.InsertTemplateAsync(new Model.Template());

            templateDbContext = new TemplateDbContext(optionsBuilder.Options);
            Assert.True(templateDbContext.Template.ToList().Count > count);
        }

        [Fact]
        public async Task GetTemplateReturnsTemplate()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TemplateDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var id = Guid.NewGuid();
            var lookup = Guid.NewGuid();
            string version = "versionHash";
            var templateDbContext = new TemplateDbContext(optionsBuilder.Options);

            templateDbContext.Template.Add(new Model.Template() {Id = id, LookupKey = lookup, Version = version });
            templateDbContext.SaveChanges();

            var templateDbContextProviderMock = new Mock<ITemplateDbContextProvider>();
            templateDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(templateDbContext);
            
            var provider = new TemplateDataProvider(templateDbContextProviderMock.Object);

            var template = await provider.GetTemplateByLookupKeyAndVersionAsync(lookup, version);

            Assert.NotNull(template);
            Assert.Equal(template.Id, id);
        }

        [Fact]
        public async Task GetTemplatesReturnsTemplates()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TemplateDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var id = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var lookup = Guid.NewGuid();
            var lookup2 = Guid.NewGuid();
            string version = "versionHash";
            var templateDbContext = new TemplateDbContext(optionsBuilder.Options);

            templateDbContext.Template.Add(new Model.Template() { Id = id, LookupKey = lookup, Version = version });
            templateDbContext.Template.Add(new Model.Template() { Id = id2, LookupKey = lookup2, Version = version });
            templateDbContext.SaveChanges();
            
            var templateDbContextProviderMock = new Mock<ITemplateDbContextProvider>();
            templateDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(templateDbContext);

            var provider = new TemplateDataProvider(templateDbContextProviderMock.Object);

            var templates = await provider.GetTemplatesAsync();

            Assert.True(templates.Count() > 0);
            Assert.NotNull(templates.FirstOrDefault(x => x.Id == id));
            Assert.NotNull(templates.FirstOrDefault(x => x.Id == id2));
        }

        [Fact]
        public async Task DeleteTemplateDeletesTemplate()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TemplateDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var id = Guid.NewGuid();
            var lookup = Guid.NewGuid();
            string version = "versionHash";
            var templateDbContext = new TemplateDbContext(optionsBuilder.Options);

            templateDbContext.Template.Add(new Model.Template() { Id = id, LookupKey = lookup, Version = version });
            templateDbContext.SaveChanges();

            var templateDbContextProviderMock = new Mock<ITemplateDbContextProvider>();
            templateDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(templateDbContext);

            var provider = new TemplateDataProvider(templateDbContextProviderMock.Object);

            var count = templateDbContext.Template.Count();

            var template = await provider.DeleteTemplateVersionAsync(lookup, version);

            templateDbContext = new TemplateDbContext(optionsBuilder.Options);
            Assert.True(count > templateDbContext.Template.Count());
            Assert.Null(templateDbContext.Template.FirstOrDefault(x => x.Id == id));
        }

        [Fact]
        public void DeleteTemplateThrowsValidationExceptionOnTemplateNotFound()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TemplateDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var lookup = Guid.NewGuid();
            string version = "versionHash";
            var templateDbContext = new TemplateDbContext(optionsBuilder.Options);

            var templateDbContextProviderMock = new Mock<ITemplateDbContextProvider>();
            templateDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(templateDbContext);

            var provider = new TemplateDataProvider(templateDbContextProviderMock.Object);

            Assert.ThrowsAsync(typeof(ValidationException), () => provider.DeleteTemplateVersionAsync(lookup, version));
        }

        [Fact]
        public void SetConnectionStringFormatsConnectionStringCorrectly()
        {
            var config = new Config();
            config.ResponseDatabaseConfig = new DatabaseConfig() { DatabaseName = "ResponsedbName", Server = "server", Port = "5432", Password = "password", User = "user" };

            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            var templateDbContext = new TemplateDbContext("connectionstring");
            templateDbContext.DatabaseConfig = config.ResponseDatabaseConfig;

            templateDbContext.SetConnectionString(optionsBuilder);

            Assert.Equal(templateDbContext.Database.GetDbConnection().ConnectionString, "Host=server;Port=5432;Database=ResponsedbName;Username=user;Password=password");
        }

        [Fact]
        public void EmptyCtorTest()
        {
            var dbContext = new TemplateDbContext();
            Assert.NotNull(dbContext);
        }

        [Fact]
        public void CheckConnectionReturnsTrue()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TemplateDbContext>();
            optionsBuilder.UseInMemoryDatabase();
            var templateDbContext = new TemplateDbContext(optionsBuilder.Options);

            var templateDbContextProviderMock = new Mock<ITemplateDbContextProvider>();
            templateDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(templateDbContext);

            var provider = new TemplateDataProvider(templateDbContextProviderMock.Object);

            var connectionValid = provider.CheckDBConnectionAsync().Result;

            Assert.False(connectionValid.ConnectionOk);
        }
    }
}
