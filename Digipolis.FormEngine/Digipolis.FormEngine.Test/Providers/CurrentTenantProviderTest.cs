﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Domain.Providers;
using Digipolis.FormEngine.Model;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Digipolis.FormEngine.Test.Providers
{
    public class CurrentTenantProviderTest
    {
        [Fact]
        public void ThrowsExceptionWhenHeadersNotContaining()
        {
            var headerDictionary = new HeaderDictionary();

            var httpRequest = new Mock<HttpRequest>();
            httpRequest.SetupGet(x => x.Headers).Returns(headerDictionary);

            var httpContextMock = new Mock<HttpContext>();
            httpContextMock.SetupGet(x => x.Request).Returns(httpRequest.Object);

            var accessor = new Mock<IHttpContextAccessor>();
            accessor.SetupGet(x => x.HttpContext).Returns(httpContextMock.Object);

            var tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(x => x.GetTenantAsync("tenantKey")).ReturnsAsync(new Tenant<Config>());

            var currentTenantProvider = new CurrentTenantProvider(accessor.Object, tenantClientMock.Object);
            Assert.ThrowsAsync(typeof(Exception), () => currentTenantProvider.GetCurrentTenantAsync(false));
        }

        [Fact]
        public void ReturnsTenant()
        {
            var headerDictionary = new HeaderDictionary();
            headerDictionary.Add(new KeyValuePair<string, Microsoft.Extensions.Primitives.StringValues>("tenantKey", "tenantKey"));

            var httpRequest = new Mock<HttpRequest>();
            httpRequest.SetupGet(x => x.Headers).Returns(headerDictionary);

            var httpContextMock = new Mock<HttpContext>();
            httpContextMock.SetupGet(x => x.Request).Returns(httpRequest.Object);

            var accessor = new Mock<IHttpContextAccessor>();
            accessor.SetupGet(x => x.HttpContext).Returns(httpContextMock.Object);

            var id = Guid.NewGuid();
            var tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(x => x.GetTenantAsync("tenantKey")).ReturnsAsync(new Tenant<Config>() { Id = id });

            var currentTenantProvider = new CurrentTenantProvider(accessor.Object, tenantClientMock.Object);
            var tenant = currentTenantProvider.GetCurrentTenantAsync().Result;

            Assert.NotNull(tenant);
            Assert.Equal(id, tenant.Id);
        }

        [Fact]
        public void ReturnsCachedTenantOnSecondRun()
        {
            var headerDictionary = new HeaderDictionary();
            headerDictionary.Add(new KeyValuePair<string, Microsoft.Extensions.Primitives.StringValues>("tenantKey", "tenantKey"));

            var httpRequest = new Mock<HttpRequest>();
            httpRequest.SetupGet(x => x.Headers).Returns(headerDictionary);

            var httpContextMock = new Mock<HttpContext>();
            httpContextMock.SetupGet(x => x.Request).Returns(httpRequest.Object);

            var accessor = new Mock<IHttpContextAccessor>();
            accessor.SetupGet(x => x.HttpContext).Returns(httpContextMock.Object);

            var id = Guid.NewGuid();
            var tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(x => x.GetTenantAsync("tenantKey")).ReturnsAsync(new Tenant<Config>() { Id = id });

            var currentTenantProvider = new CurrentTenantProvider(accessor.Object, tenantClientMock.Object);
            var tenant = currentTenantProvider.GetCurrentTenantAsync().Result;

            Assert.NotNull(tenant);
            Assert.Equal(id, tenant.Id);
        }

        [Fact]
        public void ReturnsTenantKey()
        {
            var headerDictionary = new HeaderDictionary();
            headerDictionary.Add(new KeyValuePair<string, Microsoft.Extensions.Primitives.StringValues>("tenantKey", "tenantKey"));

            var httpRequest = new Mock<HttpRequest>();
            httpRequest.SetupGet(x => x.Headers).Returns(headerDictionary);

            var httpContextMock = new Mock<HttpContext>();
            httpContextMock.SetupGet(x => x.Request).Returns(httpRequest.Object);

            var accessor = new Mock<IHttpContextAccessor>();
            accessor.SetupGet(x => x.HttpContext).Returns(httpContextMock.Object);

          
            var tenantClientMock = new Mock<ITenantClient>();
         
            var currentTenantProvider = new CurrentTenantProvider(accessor.Object, tenantClientMock.Object);
            var result = currentTenantProvider.GetCurrentTenantAsync();
            var tenantKey = currentTenantProvider.GetCurrentTenantKey();

            Assert.NotNull(tenantKey);
            Assert.Equal("tenantKey", tenantKey);
        }
    }
}
