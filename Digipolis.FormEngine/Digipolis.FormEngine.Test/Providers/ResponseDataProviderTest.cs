﻿using Digipolis.FormEngine.DataProvider.DataProviders;
using Digipolis.FormEngine.DataProvider.DbContexts;
using Digipolis.FormEngine.Model;
using Microsoft.EntityFrameworkCore;
using Moq;
using Narato.Common.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Digipolis.FormEngine.Test.Providers
{
    public class ResponseDataProviderTest
    {
        [Fact]
        public void InsertResponseAddsResponseToResponses()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var responseDbContext = new ResponseDbContext(optionsBuilder.Options);

            var responseDbContextProviderMock = new Mock<IResponseDbContextProvider>();
            responseDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(responseDbContext);

            var provider = new ResponseDataProvider(responseDbContextProviderMock.Object);

            var count = responseDbContext.Response.ToList().Count;
            var result = provider.InsertResponseAsync(new Model.Response());
            responseDbContext = new ResponseDbContext(optionsBuilder.Options);

            Assert.True(responseDbContext.Response.ToList().Count > count);
        }

        [Fact]
        public async Task UpdateResponseUpdatesResponse()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var id = Guid.NewGuid();
            var responseDbContext = new ResponseDbContext(optionsBuilder.Options);

            responseDbContext.Response.Add(new Model.Response() { Id = id, Content = "originalContent" });
            responseDbContext.SaveChanges();

            var responseDbContextProviderMock = new Mock<IResponseDbContextProvider>();
            responseDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(responseDbContext);

            var provider = new ResponseDataProvider(responseDbContextProviderMock.Object);

            var updatedResponse = await provider.UpdateResponseAsync(new Model.Response() { Id = id, Content = "updatedContent" });

            Assert.Equal("updatedContent", updatedResponse.Content);
        }

        [Fact]
        public void UpdateResponseThrowsValidationExceptionOnResponseNotFound()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            optionsBuilder.UseInMemoryDatabase();
           
            var responseDbContext = new ResponseDbContext(optionsBuilder.Options);
            var responseDbContextProviderMock = new Mock<IResponseDbContextProvider>();
            responseDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(responseDbContext);

            var provider = new ResponseDataProvider(responseDbContextProviderMock.Object);

            Assert.ThrowsAsync(typeof(ValidationException), () => provider.UpdateResponseAsync(new Model.Response() { Id = Guid.NewGuid() }));
        }

        [Fact]
        public async Task DeleteResponseDeletesResponse()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var id = Guid.NewGuid();
            var responseDbContext = new ResponseDbContext(optionsBuilder.Options);

            responseDbContext.Response.Add(new Model.Response() { Id = id, Content = "originalContent" });
            responseDbContext.SaveChanges();
            var responseDbContextProviderMock = new Mock<IResponseDbContextProvider>();
            responseDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(responseDbContext);

            var provider = new ResponseDataProvider(responseDbContextProviderMock.Object);

            var count = responseDbContext.Response.Count();
            var updatedResponse = await provider.DeleteResponseAsync(id);

            responseDbContext = new ResponseDbContext(optionsBuilder.Options);
            Assert.True(count > responseDbContext.Response.Count());
            Assert.Null(responseDbContext.Response.FirstOrDefault(x => x.Id == id));
        }

        [Fact]
        public void DeleteResponseThrowsValidationExceptionOnResponseNotFound()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var responseDbContext = new ResponseDbContext(optionsBuilder.Options);

            var responseDbContextProviderMock = new Mock<IResponseDbContextProvider>();
            responseDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(responseDbContext);

            var provider = new ResponseDataProvider(responseDbContextProviderMock.Object);

            Assert.ThrowsAsync(typeof(ValidationException), () => provider.DeleteResponseAsync(Guid.NewGuid()));
        }

        [Fact]
        public async Task GetResponseReturnResponse()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var id = Guid.NewGuid();
            var responseDbContext = new ResponseDbContext(optionsBuilder.Options);

            responseDbContext.Response.Add(new Model.Response() { Id = id });
            responseDbContext.SaveChanges();

            var responseDbContextProviderMock = new Mock<IResponseDbContextProvider>();
            responseDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(responseDbContext);

            var provider = new ResponseDataProvider(responseDbContextProviderMock.Object);

            var response = await provider.GetResponseByIdAsync(id);

            Assert.NotNull(response);
            Assert.Equal(response.Id, id);
        }

        [Fact]
        public async Task GetResponseByTemplateReturnResponseForTemplate()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            var id = Guid.NewGuid();
            var responseDbContext = new ResponseDbContext(optionsBuilder.Options);
            var templateLookypKey = Guid.NewGuid();

            responseDbContext.Response.Add(new Model.Response() { Id = id, TemplateLookupKey = templateLookypKey, TemplateVersion="123"});
            responseDbContext.SaveChanges();

            var responseDbContextProviderMock = new Mock<IResponseDbContextProvider>();
            responseDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(responseDbContext);

            var provider = new ResponseDataProvider(responseDbContextProviderMock.Object);

            var responses = await provider.GetResponseByTemplateAsync(templateLookypKey, "123", 1, 10);

            Assert.True(responses.Count() > 0);
        }

        [Fact]
        public void SetConnectionStringFormatsConnectionStringCorrectly()
        {
            var config = new Model.Config();
            config.ResponseDatabaseConfig = new DatabaseConfig() { DatabaseName = "ResponsedbName", Server = "server", Port = "5432", Password = "password", User = "user" };
           
            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            var responseDbContext = new ResponseDbContext("connectionstring");
            responseDbContext.DatabaseConfig = config.ResponseDatabaseConfig;

            responseDbContext.SetConnectionString(optionsBuilder);
            
            Assert.Equal(responseDbContext.Database.GetDbConnection().ConnectionString, "Host=server;Port=5432;Database=ResponsedbName;Username=user;Password=password");
        }

        [Fact]
        public void EmptyCtorTest()
        {
            var dbContext = new ResponseDbContext();
            Assert.NotNull(dbContext);
        }

        [Fact]
        public void CheckConnectionReturnsTrue()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ResponseDbContext>();
            optionsBuilder.UseInMemoryDatabase();
            var responseDbContext = new ResponseDbContext(optionsBuilder.Options);

            var responseDbContextProviderMock = new Mock<IResponseDbContextProvider>();
            responseDbContextProviderMock.Setup(x => x.ProvideAsync()).ReturnsAsync(responseDbContext);

            var provider = new ResponseDataProvider(responseDbContextProviderMock.Object);

            var connectionValid = provider.CheckDBConnectionAsync();

            Assert.False(connectionValid.Result.ConnectionOk);
        }




    }
}
