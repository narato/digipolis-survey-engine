﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Domain.Providers;
using Digipolis.FormEngine.Model;
using Moq;
using Xunit;

namespace Digipolis.FormEngine.Test.Providers
{
    public class TemplateDbContextProviderTest
    {
        [Fact]
        public void CtorTest()
        {
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();

            var templateDbContextProvider = new TemplateDbContextProvider(currentTenantProviderMock.Object);

            Assert.NotNull(templateDbContextProvider);
        }

        [Fact]
        public void ProvideReturnDbContext()
        {
            var tenant = new Tenant<Config>() { Config = new Config { TemplateDatabaseConfig = new DatabaseConfig() } };
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(tenant);

            var templateDbContextProvider = new TemplateDbContextProvider(currentTenantProviderMock.Object);

            var context = templateDbContextProvider.ProvideAsync().Result;
            currentTenantProviderMock.Verify(x => x.GetCurrentTenantAsync(false), Times.Once);
            Assert.Equal(tenant.Config.TemplateDatabaseConfig, context.DatabaseConfig);
        }
    }
}
