﻿using Digipolis.FormEngine.Domain.Mappers;
using Digipolis.FormEngine.Model;
using Newtonsoft.Json;
using System;
using Xunit;

namespace Digipolis.FormEngine.Test.Mappers
{
    public class ConfigMapperTest
    {
        [Fact]
        public void MapperReturnsATentantWithCorrectConfigObject()
        {
            var tenantIn = new Digipolis.Common.Tenancy.MappedModels.Tenant<Config>();
            var id = Guid.NewGuid();
            tenantIn.Id = id;
            tenantIn.Key = "TenantKey";
            tenantIn.Name = "TenantName";
            tenantIn.Status = "Status";
            tenantIn.Description = "Description";

            var config = new Config();
            config.ResponseDatabaseConfig = new DatabaseConfig() { DatabaseName = "ResponsedbName", Server="server", Port="5432", Password="password", User="user" };
            config.TemplateDatabaseConfig = new DatabaseConfig() { DatabaseName = "TemplatedbName", Server = "server", Port = "5432", Password = "password", User = "user" };

            tenantIn.Config = config;

            var configJson = JsonConvert.SerializeObject(config);

            var mapper = new ConfigMapper<Config>();
            var tenantOut = mapper.Map(tenantIn);

            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Id, id);
            Assert.Equal(tenantOut.Key, "TenantKey");
            Assert.Equal(tenantOut.Name, "TenantName");
            Assert.Equal(tenantOut.Status, "Status");
            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Config, configJson);
        }

        [Fact]
        public void MapperReturnsATentantWithConfigJson()
        {
            var tenantIn = new Digipolis.Common.Tenancy.Models.Tenant();
            var id = Guid.NewGuid();
            tenantIn.Id = id;
            tenantIn.Key = "TenantKey";
            tenantIn.Name = "TenantName";
            tenantIn.Status = "Status";
            tenantIn.Description = "Description";

            var config = new Config();
            config.ResponseDatabaseConfig = new DatabaseConfig() { DatabaseName = "ResponsedbName", Server = "server", Port = "5432", Password = "password", User = "user" };
            config.TemplateDatabaseConfig = new DatabaseConfig() { DatabaseName = "TemplatedbName", Server = "server", Port = "5432", Password = "password", User = "user" };

            tenantIn.Config = JsonConvert.SerializeObject(config);

            var mapper = new ConfigMapper<Config>();
            var tenantOut = mapper.Map(tenantIn);

            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Id, id);
            Assert.Equal(tenantOut.Key, "TenantKey");
            Assert.Equal(tenantOut.Name, "TenantName");
            Assert.Equal(tenantOut.Status, "Status");
            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Config.ResponseDatabaseConfig.DatabaseName, "ResponsedbName");
            Assert.Equal(tenantOut.Config.TemplateDatabaseConfig.DatabaseName, "TemplatedbName");
        }
    }
}
