﻿using Digipolis.FormEngine.Model.RouteValues;
using System;
using Xunit;

namespace Digipolis.FormEngine.Test.RouteValues
{
    public class RouteValuesTest
    {
        [Fact]
        public void TestResponseRouteValues()
        {
            var id = Guid.NewGuid();
            var routeValues = new PostResponseRouteValues() { Id = id, Controller = "Controller", TenantKey = "tenantKey" };
            Assert.Equal(id, routeValues.Id);
            Assert.Equal("Controller", routeValues.Controller);
            Assert.Equal("tenantKey", routeValues.TenantKey);
        }


        [Fact]
        public void TestTemplateRouteValues()
        {
            var lookupKey = Guid.NewGuid();
            var routeValues = new Model.RouteValues.PostTemplateRouteValues() { TemplateLookupKey= lookupKey, Controller = "Controller", TenantKey = "tenantKey", Version="version" };
            Assert.Equal(lookupKey, routeValues.TemplateLookupKey);
            Assert.Equal("Controller", routeValues.Controller);
            Assert.Equal("tenantKey", routeValues.TenantKey);
            Assert.Equal("version", routeValues.Version);
        }
    }
}
