﻿using System;
using System.Collections.Generic;
using Moq;
using Xunit;
using Digipolis.FormEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Digipolis.FormEngine.Template.API.Controllers;
using Narato.Common.Factory;
using Narato.Common.Models;
using Digipolis.FormEngine.Model.SwaggerResponseModels;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Test.Controllers
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class TemplateControllerTest
    {
        [Fact]
        public void GetReturnsAnActionResult()
        {
            Mock<ITemplateManager> managerMock = new Mock<ITemplateManager>();
            Mock<IResponseFactory> factoryMock = new Mock<IResponseFactory>();
            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<Model.Template>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var lookupKey = Guid.NewGuid();
            string version = "versionhash";
            var controller = new TemplatesController(managerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAsync(lookupKey, version, "tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetWithMissingTenantIdReturnsBadRequest()
        {
            Mock<ITemplateManager> managerMock = new Mock<ITemplateManager>();
            Mock<IResponseFactory> factoryMock = new Mock<IResponseFactory>();
            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<MissingParam>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new TemplatesController(managerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAsync(Guid.NewGuid(), "versionHash", string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void PostWithoutTenantIdReturnsBadRequest()
        {
            Mock<ITemplateManager> managerMock = new Mock<ITemplateManager>();
            Mock<IResponseFactory> factoryMock = new Mock<IResponseFactory>();
            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new TemplatesController(managerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.PostAsync(new PostTemplate(), string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }
    }
}
