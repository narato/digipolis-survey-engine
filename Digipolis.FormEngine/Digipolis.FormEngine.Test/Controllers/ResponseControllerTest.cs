﻿using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Model.SwaggerResponseModels;
using Digipolis.FormEngine.Response.API.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Digipolis.FormEngine.Test.Controllers
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class ResponseControllerTest
    {
        [Fact]
        public void GetReturnsAnActionResult()
        {
            Mock<IResponseManager> managerMock = new Mock<IResponseManager>();
            Mock<IResponseFactory> factoryMock = new Mock<IResponseFactory>();
            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<Model.Response>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));
            var id = Guid.NewGuid();
            var controller = new ResponsesController(managerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAsync(id, "tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        //[Fact]
        //public void GetReturnsAnActionResult()
        //{
        //    // Arrange
        //    var tenantId = Guid.NewGuid();
        //    var id = Guid.NewGuid();
        //    var response = new Model.Response();
        //    var rawQuerystring = $"?tenant={tenantId}";
        //    var queryString = new QueryString(rawQuerystring);
        //    var path = string.Empty;

        //    var mockHttpContextMock = new Mock<HttpContext>();
        //    var mockRequestMock = new Mock<HttpRequest>();
        //    var managerMock = new Mock<IResponseManager>();
        //    var responseFactory = new ResponseFactory();

        //    mockRequestMock.Setup(x => x.QueryString).Returns(queryString);
        //    mockRequestMock.Setup(x => x.Path).Returns(path);
        //    mockHttpContextMock.Setup(h => h.Request).Returns(mockRequestMock.Object);
        //    managerMock.Setup(m => m.GetResponseById(id)).Returns(response);

        //    var controller = new ResponseController(managerMock.Object, responseFactory)
        //    {
        //        ControllerContext = new ControllerContext
        //        {
        //            HttpContext = mockHttpContextMock.Object
        //        }
        //    };

        //    // Act
        //    var actionResult = controller.Get(id, tenantId);

        //    // Assert
        //    Assert.NotNull(actionResult);
        //    var result = ((Response<Model.Response>)((ObjectResult)actionResult).Value);
        //    Assert.Equal(path + rawQuerystring, result.Self);
        //    managerMock.Verify(x => x.GetResponseById(id));
        //}

        [Fact]
        public void GetWithMissingTenantIdReturnsBadRequest()
        {
            Mock<IResponseManager> managerMock = new Mock<IResponseManager>();
            Mock<IResponseFactory> factoryMock = new Mock<IResponseFactory>();
            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<MissingParam>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new ResponsesController(managerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAsync(Guid.NewGuid(), string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void PostWithoutTenantIdReturnsBadRequest()
        {
            Mock<IResponseManager> managerMock = new Mock<IResponseManager>();
            Mock<IResponseFactory> factoryMock = new Mock<IResponseFactory>();
            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new ResponsesController(managerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.PostAsync(new PostResponse(), string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);

        }


    }
}
