﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Domain.Clients;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.Model.Validator;
using Moq;
using Narato.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Xunit;

namespace Digipolis.FormEngine.Test.Validator
{
    public class TemplateValidatorTest
    {
        [Fact]
        public void ValidateForInsertThrowsValidationExceptionOnMissingName()
        {
            var templateValidator = new TemplateValidator(null);
            Assert.Throws(typeof(ValidationException), () => templateValidator.ValidatrForInsert(new Model.Template { Content="content" }));
        }

        [Fact]
        public void ValidateForInsertThrowsValidationExceptionOnMissingContent()
        {
            var templateValidator = new TemplateValidator(null);
            Assert.Throws(typeof(ValidationException), () => templateValidator.ValidatrForInsert(new Model.Template { Name = "name"}));
        }

        [Fact]
        public void ValidateForDeleteThrowsValidationExceptionOnTemplateWithExistingResponses()
        {
            var lookupKey = Guid.NewGuid();
            var version = "123";

            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(new Tenant<Config>());

            var responseClientMock = new Mock<ResponseClient>(new HttpClient(), currentTenantProviderMock.Object);
            responseClientMock.Setup(x => x.GetResponsesForTemplate(lookupKey, version)).Returns(new List<Model.Response>() { new Model.Response()});

            var templateValidator = new TemplateValidator(responseClientMock.Object);
            Assert.Throws(typeof(ValidationException), () => templateValidator.ValidateForDelete(lookupKey, version));
        }
    }
}
