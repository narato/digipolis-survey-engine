﻿using Digipolis.FormEngine.Model.Validator;
using Narato.Common.Exceptions;
using System;
using Xunit;

namespace Digipolis.FormEngine.Test.Validator
{
    public class ResponseValidatorTest
    {
        [Fact]
        public void ValidateForInsertThrowsValidationExceptionOnMissingTemplateLookupKey()
        {
            var responseValidator = new ResponseValidator();
            Assert.Throws(typeof(ValidationException) , () => responseValidator.ValidateForInsert(new Model.Response { Content = "content", TemplateVersion = "123" }));
        }

        [Fact]
        public void ValidateForInsertThrowsValidationExceptionOnMissingTemplateVersion()
        {
            var responseValidator = new ResponseValidator();
            Assert.Throws(typeof(ValidationException), () => responseValidator.ValidateForInsert(new Model.Response { Content = "content", TemplateLookupKey = Guid.NewGuid() }));
        }

        [Fact]
        public void ValidateForInsertThrowsValidationExceptionOnMissingContent()
        {
            var responseValidator = new ResponseValidator();
            Assert.Throws(typeof(ValidationException), () => responseValidator.ValidateForInsert(new Model.Response { TemplateVersion = "123", TemplateLookupKey = Guid.NewGuid() }));
        }

        [Fact]
        public void ValidateForUpdateThrowsValidationExceptionOnMissingId()
        {
            var responseValidator = new ResponseValidator();
            Assert.Throws(typeof(ValidationException), () => responseValidator.ValidateForUpdate(new Model.Response {Content = "content", TemplateVersion = "123", TemplateLookupKey = Guid.NewGuid() }));
        }

        [Fact]
        public void ValidateForUpdateThrowsValidationExceptionOnMissingTemplateLookupKey()
        {
            var responseValidator = new ResponseValidator();
            Assert.Throws(typeof(ValidationException), () => responseValidator.ValidateForUpdate(new Model.Response { Id = Guid.NewGuid(), Content = "content", TemplateVersion = "123" }));
        }

        [Fact]
        public void ValidateForUpdateThrowsValidationExceptionOnMissingTemplateVersion()
        {
            var responseValidator = new ResponseValidator();
            Assert.Throws(typeof(ValidationException), () => responseValidator.ValidateForUpdate(new Model.Response { Id = Guid.NewGuid(), Content = "content", TemplateLookupKey = Guid.NewGuid() }));
        }

        [Fact]
        public void ValidateForInsertUpdateValidationExceptionOnMissingContent()
        {
            var responseValidator = new ResponseValidator();
            Assert.Throws(typeof(ValidationException), () => responseValidator.ValidateForUpdate(new Model.Response { Id = Guid.NewGuid(), TemplateVersion = "123", TemplateLookupKey = Guid.NewGuid() }));
        }
    }
}
