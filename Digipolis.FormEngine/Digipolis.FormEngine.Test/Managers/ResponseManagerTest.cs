﻿using Digipolis.Common.Events;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Common.Models;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Domain.Managers;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.Model.Validator.Interfaces;
using Microsoft.Extensions.Options;
using Moq;
using System;
using Xunit;

namespace Digipolis.FormEngine.Test.Managers
{
    public class ResponseManagerTest
    {
        [Fact]
        public void GetReturns1Response()
        {
            Guid responseId = Guid.NewGuid();

            Mock<IResponseDbContext> responseDbContextMock = new Mock<IResponseDbContext>();
            Mock<IEventPublisher> eventPublisherMock = new Mock<IEventPublisher>();

            Mock<IResponseDataProvider> responseDataProviderMoq = new Mock<IResponseDataProvider>();
            responseDataProviderMoq.Setup(rm => rm.GetResponseByIdAsync(responseId)).ReturnsAsync(new Model.Response());           

            Mock<IResponseValidator> validatorMock = new Mock<IResponseValidator>();

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(new Tenant<Config>());

            Mock<IOptions<EngineConfiguration>> configurationOptions = new Mock<IOptions<EngineConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EngineConfiguration() {UseEventhandler = true });

            var manager = new ResponseManager(configurationOptions.Object, responseDataProviderMoq.Object, validatorMock.Object, eventPublisherMock.Object, tenantProviderMock.Object);

            var response = manager.GetResponseByIdAsync(responseId).Result;

            Assert.NotNull(response);
            Assert.IsType(typeof(Model.Response), response);
        }

        [Fact]
        public void InsertCallDataProviderInsert()
        {
            var response = new Model.Response();
            Mock<IResponseDbContext> responseDbContextMock = new Mock<IResponseDbContext>();
            Mock<IEventPublisher> eventPublisherMock = new Mock<IEventPublisher>();
            eventPublisherMock.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<object>()));
            Mock <IResponseDataProvider> responseDataProviderMoq = new Mock<IResponseDataProvider>();
            responseDataProviderMoq.Setup(rm => rm.InsertResponseAsync(response)).ReturnsAsync(new Model.Response());

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(new Tenant<Config> { Key = "tenankey", Config = new Config { EventNamespace = "eventNameSpace", EventOwnerKey = "eventOwnerKey" } });
            
            Mock<IResponseValidator> validatorMock = new Mock<IResponseValidator>();
            validatorMock.Setup(v => v.ValidateForInsert(response));
            
            Mock<IOptions<EngineConfiguration>> configurationOptions = new Mock<IOptions<EngineConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EngineConfiguration() { UseEventhandler = true });

            var manager = new ResponseManager(configurationOptions.Object, responseDataProviderMoq.Object, validatorMock.Object, eventPublisherMock.Object, tenantProviderMock.Object);

            var result = manager.InsertResponseAsync(response);

            responseDataProviderMoq.Verify(x => x.InsertResponseAsync(response), Times.Once);
            eventPublisherMock.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<object>()), Times.Once);
        }
    }
}
