﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Domain.Managers;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.Model.Validator.Interfaces;
using Moq;
using Narato.Common.Checksum;
using Narato.Common.Models;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Digipolis.FormEngine.Test.Managers
{
    public class TemplateManagerTest
    {
        [Fact]
        public void GetReturns1Template()
        {
            Guid templatelookup = Guid.NewGuid();
            string version = "versionHash";

            Mock<ITemplateDbContext> templateDbContextMock = new Mock<ITemplateDbContext>();

            Mock<ITemplateDataProvider> templateDataProviderMoq = new Mock<ITemplateDataProvider>();
            templateDataProviderMoq.Setup(rm => rm.GetTemplateByLookupKeyAndVersionAsync(templatelookup, version)).ReturnsAsync(new Model.Template());

            Mock<ITemplateValidator> validatorMock = new Mock<ITemplateValidator>();

            Mock<ITenantClient> tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(tcm => tcm.GetTenantAsync("tenantShortName")).Returns(Task.FromResult(new Tenant<Config> { Config = new Config() }));
            var manager = new TemplateManager(templateDataProviderMoq.Object, validatorMock.Object, tenantClientMock.Object, new ChecksumCalculator(), new JSchema());

            Model.Template template = manager.GetTemplateByLookupKeyAndVersionAsync(templatelookup, version).Result;

            Assert.NotNull(template);
            Assert.IsType(typeof(Model.Template), template);
        }

        [Fact]
        public void GetForTenantReturns1Template()
        {
            Guid templatelookup = Guid.NewGuid();

            Mock<ITemplateDbContext> templateDbContextMock = new Mock<ITemplateDbContext>();

            Mock<ITemplateDataProvider> templateDataProviderMoq = new Mock<ITemplateDataProvider>();
            templateDataProviderMoq.Setup(rm => rm.GetTemplatesAsync(1, 10)).ReturnsAsync(new List<Model.Template> { new Model.Template() });

            Mock<ITemplateValidator> validatorMock = new Mock<ITemplateValidator>();

            Mock<ITenantClient> tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(tcm => tcm.GetTenantAsync("tenantKey")).Returns(Task.FromResult( new Tenant<Config> { Config = new Config() }));
            var manager = new TemplateManager(templateDataProviderMoq.Object, validatorMock.Object, tenantClientMock.Object, new ChecksumCalculator(), new JSchema());

            PagedCollectionResponse<IEnumerable<Model.Template>> response = manager.GetTemplatesForTenantAsync(1, 10).Result;

            Assert.NotNull(response.Data);
            Assert.True(response.Data.ToList().Count > 0);
        }

        [Fact]
        public void DeleteTemplateCallsDeleteOnDataProvider()
        {
            Guid templatelookup = Guid.NewGuid();

            Mock<ITemplateDbContext> templateDbContextMock = new Mock<ITemplateDbContext>();

            Mock<ITemplateDataProvider> templateDataProviderMoq = new Mock<ITemplateDataProvider>();
            templateDataProviderMoq.Setup(rm => rm.GetTemplatesAsync(1,10)).ReturnsAsync(new List<Model.Template> { new Model.Template() });

            Mock<ITemplateValidator> validatorMock = new Mock<ITemplateValidator>();

            Mock<ITenantClient> tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(tcm => tcm.GetTenantAsync("tenantKey")).Returns(Task.FromResult(new Tenant<Config> { Config = new Config() }));
            var manager = new TemplateManager(templateDataProviderMoq.Object, validatorMock.Object, tenantClientMock.Object, new ChecksumCalculator(), new JSchema());

            PagedCollectionResponse<IEnumerable<Model.Template>> response = manager.GetTemplatesForTenantAsync(1, 10).Result;

            Assert.NotNull(response.Data);
            Assert.True(response.Data.ToList().Count > 0);
        }

        [Fact]
        public void InsertCallDataProviderInsert()
        {
            //var lookupKey = Guid.NewGuid();
            //string version = "versionHash";
            //string tenant = "tenantKey";

            //Mock<ITemplateDbContext> templateDbContextMock = new Mock<ITemplateDbContext>();
            //Mock<ITemplateDataProvider> templateDataProviderMoq = new Mock<ITemplateDataProvider>();

            //templateDataProviderMoq.Setup(rm => rm.DeleteTemplateVersion(lookupKey, version)).Returns(new Model.Template());
            //templateDataProviderMoq.SetupGet(rm => rm.TemplateDbContext).Returns(templateDbContextMock.Object);

            //Mock<ITemplateValidator> validatorMock = new Mock<ITemplateValidator>();
            //validatorMock.Setup(v => v.ValidateForDelete(template));

            //Mock <ITenantClient> tenantClientMock = new Mock<ITenantClient>();
            //tenantClientMock.Setup(tcm => tcm.GetTenant("tenantShortName")).Returns(new Tenant<Config> { Config = new Config() });
            //var manager = new TemplateManager(templateDataProviderMoq.Object, validatorMock.Object, tenantClientMock.Object);


            //manager.DeleteTemplate(template);

            //templateDataProviderMoq.Verify(x => x.InsertTemplate(template), Times.Once);
        }
    }
}

