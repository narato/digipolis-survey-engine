﻿using Digipolis.Common.Events;
using Digipolis.Common.Status.Models;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Common.Models;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Domain.Managers;
using Digipolis.FormEngine.Model;
using Microsoft.Extensions.Options;
using Moq;
using System;
using Xunit;

namespace Digipolis.FormEngine.Test.Managers
{
    public class SystemStatusResponseManagerTest
    {
        [Fact]
        public void GetReturns1SystemStatus()
        {
            Guid responseId = Guid.NewGuid();

            Mock<IResponseDbContext> responseDbContextMock = new Mock<IResponseDbContext>();

            Mock<IResponseDataProvider> responseDataProviderMoq = new Mock<IResponseDataProvider>();
            responseDataProviderMoq.Setup(rm => rm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(true));

            Mock<IOptions<SystemConfiguration>> systemStatusConfigruationMock = new Mock<IOptions<SystemConfiguration>>();
            systemStatusConfigruationMock.SetupGet(x => x.Value).Returns(new SystemConfiguration());

            Mock<IOptions<EngineConfiguration>> configurationOptions = new Mock<IOptions<EngineConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EngineConfiguration());

            Mock<IEventhandlerPing> eventHandlerAdministratorMock = new Mock<IEventhandlerPing>();
            eventHandlerAdministratorMock.Setup(x => x.Ping()).Returns(true);

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<Digipolis.Common.Tenancy.Providers.ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(new Tenant<Config>());

            var manager = new SystemStatusResponseManager(systemStatusConfigruationMock.Object, configurationOptions.Object,  responseDataProviderMoq.Object, eventHandlerAdministratorMock.Object, tenantProviderMock.Object);

            SystemStatus status = manager.GetStatus();

            Assert.NotNull(status);
            Assert.True(status.Up);
            Assert.IsType(typeof(SystemStatus), status);
        }

        [Fact]
        public void GetReturns1SystemStatusWithFeedbackAboutPostGresDatabase()
        {
            Guid responseId = Guid.NewGuid();

            Mock<IResponseDbContext> responseDbContextMock = new Mock<IResponseDbContext>();

            Mock<IResponseDataProvider> responseDataProviderMoq = new Mock<IResponseDataProvider>();
            responseDataProviderMoq.Setup(rm => rm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(false, new Exception()));
            
            Mock<IOptions<EngineConfiguration>> configurationOptions = new Mock<IOptions<EngineConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EngineConfiguration());

            Mock<IOptions<SystemConfiguration>> systemStatusConfigruationMock = new Mock<IOptions<SystemConfiguration>>();
            systemStatusConfigruationMock.SetupGet(x => x.Value).Returns(new SystemConfiguration());

            Mock<IEventhandlerPing> eventHandlerAdministratorMock = new Mock<IEventhandlerPing>();
            eventHandlerAdministratorMock.Setup(x => x.Ping()).Returns(true);

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<Digipolis.Common.Tenancy.Providers.ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(new Tenant<Config>() { Config = new Config { ResponseDatabaseConfig = new DatabaseConfig { DatabaseName = "dbName", Server = "Server", Port="1234", Password="Password", User="user"} } });

            var manager = new SystemStatusResponseManager(systemStatusConfigruationMock.Object, configurationOptions.Object, responseDataProviderMoq.Object, eventHandlerAdministratorMock.Object, tenantProviderMock.Object);

            SystemStatus status = manager.GetStatus();

            Assert.NotNull(status);
            Assert.False(status.Up);
            Assert.Equal(1, status.Components.Count);
            Assert.Equal("Postgres Database", status.Components[0].Name);
            Assert.IsType(typeof(SystemStatus), status);
        }

        [Fact]
        public void GetReturns1SystemStatusWithFeedbackAboutAdminAPIOnException()
        {
            Guid responseId = Guid.NewGuid();

            Mock<IResponseDbContext> responseDbContextMock = new Mock<IResponseDbContext>();

            Mock<IResponseDataProvider> responseDataProviderMoq = new Mock<IResponseDataProvider>();
            responseDataProviderMoq.Setup(rm => rm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(true));

            Mock<IOptions<EngineConfiguration>> configurationOptions = new Mock<IOptions<EngineConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EngineConfiguration());

            Mock<IOptions<SystemConfiguration>> systemStatusConfigruationMock = new Mock<IOptions<SystemConfiguration>>();
            systemStatusConfigruationMock.SetupGet(x => x.Value).Returns(new SystemConfiguration());

            Mock<IEventhandlerPing> eventHandlerPingMock = new Mock<IEventhandlerPing>();
            eventHandlerPingMock.Setup(x => x.Ping()).Returns(true);

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<Digipolis.Common.Tenancy.Providers.ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).Throws(new Exception());

            var manager = new SystemStatusResponseManager(systemStatusConfigruationMock.Object, configurationOptions.Object, responseDataProviderMoq.Object, eventHandlerPingMock.Object, tenantProviderMock.Object);

            SystemStatus status = manager.GetStatus();

            Assert.NotNull(status);
            Assert.False(status.Up);
            Assert.Equal(1, status.Components.Count);
            Assert.Equal("Form engine Admin api", status.Components[0].Name);
            Assert.IsType(typeof(SystemStatus), status);
        }

        [Fact]
        public void GetReturns1SystemStatusWithFeedbackAboutAdminAPIOnUnauthorizedAccexException()
        {
            Guid responseId = Guid.NewGuid();

            Mock<IResponseDbContext> responseDbContextMock = new Mock<IResponseDbContext>();

            Mock<IResponseDataProvider> responseDataProviderMoq = new Mock<IResponseDataProvider>();
            responseDataProviderMoq.Setup(rm => rm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(true));
  
            Mock<IOptions<EngineConfiguration>> configurationOptions = new Mock<IOptions<EngineConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new EngineConfiguration());

            Mock<IOptions<SystemConfiguration>> systemStatusConfigruationMock = new Mock<IOptions<SystemConfiguration>>();
            systemStatusConfigruationMock.SetupGet(x => x.Value).Returns(new SystemConfiguration());

            Mock<IEventhandlerPing> eventHandlerAdministratorMock = new Mock<IEventhandlerPing>();
            eventHandlerAdministratorMock.Setup(x => x.Ping()).Returns(true);

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<Digipolis.Common.Tenancy.Providers.ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).Throws(new UnauthorizedAccessException());

            var manager = new SystemStatusResponseManager(systemStatusConfigruationMock.Object, configurationOptions.Object,  responseDataProviderMoq.Object, eventHandlerAdministratorMock.Object, tenantProviderMock.Object);

            SystemStatus status = manager.GetStatus();

            Assert.NotNull(status);
            Assert.False(status.Up);
            Assert.Equal(1, status.Components.Count);
            Assert.Equal("Form engine Admin api", status.Components[0].Name);
            Assert.IsType(typeof(SystemStatus), status);
        }
    }
}
