﻿using Digipolis.Common.Status.Models;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Domain.Managers;
using Digipolis.FormEngine.Model;
using Microsoft.Extensions.Options;
using Moq;
using System;
using Xunit;

namespace Digipolis.FormEngine.Test.Managers
{
    public class SystemStatusTemplateManagerTest
    {
        [Fact]
        public void GetReturns1SystemStatus()
        {
            Guid id = Guid.NewGuid();

            Mock<ITemplateDbContext> templateDbContextMock = new Mock<ITemplateDbContext>();

            Mock<ITemplateDataProvider> templateDataProviderMoq = new Mock<ITemplateDataProvider>();
            templateDataProviderMoq.Setup(rm => rm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(true));
            
            Mock<IOptions<SystemConfiguration>> configurationOptions = new Mock<IOptions<SystemConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new SystemConfiguration());

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<Digipolis.Common.Tenancy.Providers.ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(new Tenant<Config>());

            var manager = new SystemStatusTemplateManager(configurationOptions.Object, tenantProviderMock.Object, templateDataProviderMoq.Object);

            SystemStatus status = manager.GetStatusAsync().Result;

            Assert.NotNull(status);
            Assert.True(status.Up);
            Assert.IsType(typeof(SystemStatus), status);
        }


        [Fact]
        public void GetReturns1SystemStatusWithFeedbackAboutPostGresDatabase()
        {
            Guid id = Guid.NewGuid();

            Mock<ITemplateDbContext> templateDbContextMock = new Mock<ITemplateDbContext>();

            Mock<ITemplateDataProvider> templateDataProviderMoq = new Mock<ITemplateDataProvider>();
            templateDataProviderMoq.Setup(rm => rm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(false, new Exception()));

            Mock<IOptions<SystemConfiguration>> configurationOptions = new Mock<IOptions<SystemConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new SystemConfiguration());

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(new Tenant<Config>() { Config = new Config { TemplateDatabaseConfig = new DatabaseConfig { DatabaseName = "dbName", Server = "Server", Port = "1234", Password = "Password", User = "user" } } });


            var manager = new SystemStatusTemplateManager(configurationOptions.Object, tenantProviderMock.Object, templateDataProviderMoq.Object);

            SystemStatus status = manager.GetStatusAsync().Result;

            Assert.NotNull(status);
            Assert.False(status.Up);
            Assert.Equal(1, status.Components.Count);
            Assert.Equal("Postgres Database", status.Components[0].Name);
            Assert.IsType(typeof(SystemStatus), status);
        }

        [Fact]
        public void GetReturns1SystemStatusWithFeedbackAboutAdminAPIOnExceptionWhileRetrievingTenantKey()
        {
            Guid id = Guid.NewGuid();

            Mock<ITemplateDbContext> templateDbContextMock = new Mock<ITemplateDbContext>();

            Mock<ITemplateDataProvider> templateDataProviderMoq = new Mock<ITemplateDataProvider>();
            templateDataProviderMoq.Setup(rm => rm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(true));
           
            Mock<IOptions<SystemConfiguration>> configurationOptions = new Mock<IOptions<SystemConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new SystemConfiguration());

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).ReturnsAsync(new Tenant<Config>());
            tenantProviderMock.Setup(x => x.GetCurrentTenantKey()).Throws(new Exception());

            var manager = new SystemStatusTemplateManager(configurationOptions.Object, tenantProviderMock.Object, templateDataProviderMoq.Object);

            SystemStatus status = manager.GetStatusAsync().Result;

            Assert.NotNull(status);
            Assert.False(status.Up);
            Assert.Equal(1, status.Components.Count);
            Assert.Equal("Form engine Admin api", status.Components[0].Name);
            Assert.IsType(typeof(SystemStatus), status);
        }

        [Fact]
        public void GetReturns1SystemStatusWithFeedbackAboutAdminAPIOnUnauthorizedAccexException()
        {
            Guid id = Guid.NewGuid();

            Mock<ITemplateDbContext> templateDbContextMock = new Mock<ITemplateDbContext>();

            Mock<ITemplateDataProvider> templateDataProviderMoq = new Mock<ITemplateDataProvider>();
            templateDataProviderMoq.Setup(rm => rm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(true));

            Mock<IOptions<SystemConfiguration>> configurationOptions = new Mock<IOptions<SystemConfiguration>>();
            configurationOptions.SetupGet(x => x.Value).Returns(new SystemConfiguration());

            Mock<ICurrentTenantProvider<Config>> tenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            tenantProviderMock.Setup(x => x.GetCurrentTenantAsync(false)).Throws(new UnauthorizedAccessException());

            var manager = new SystemStatusTemplateManager(configurationOptions.Object, tenantProviderMock.Object, templateDataProviderMoq.Object);

            SystemStatus status = manager.GetStatusAsync().Result;

            Assert.NotNull(status);
            Assert.False(status.Up);
            Assert.Equal(1, status.Components.Count);
            Assert.Equal("Form engine Admin api", status.Components[0].Name);
            Assert.IsType(typeof(SystemStatus), status);
        }

    }
}
