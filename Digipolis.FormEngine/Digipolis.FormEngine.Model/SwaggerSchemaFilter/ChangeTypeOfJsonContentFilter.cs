﻿using Newtonsoft.Json;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Model.SwaggerSchemaFilter
{
    public class ChangeTypeOfJsonContentFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            foreach (var definition in swaggerDoc.Definitions)
            {
                var type = Type.GetType($"Digipolis.FormEngine.Model.SwaggerResponseModels.{definition.Key}");
                if (type == null)
                {
                    type = Type.GetType($"Digipolis.FormEngine.Model.{definition.Key}");
                }

                if (type != null && type.GetInterfaces().Contains(typeof(IJsonContentHolder)))
                {
                    if (definition.Value.Properties.Any(x => x.Key.ToLower() == "content"))
                        definition.Value.Properties.First(x => x.Key.ToLower() == "content").Value.Type = "object";
                }
            }
        }
    }
}
