﻿using Newtonsoft.Json;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;
using System;
using System.Linq;
using System.Reflection;

namespace Digipolis.FormEngine.Model.SwaggerSchemaFilter
{
    public class OmitIgnoredPropertiesDocumentfilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            foreach (var definition in swaggerDoc.Definitions)
            {
                var type = Type.GetType($"Digipolis.FormEngine.Model.SwaggerResponseModels.{definition.Key}");
                if (type != null)
                {
                    var ignoredProperties = type.GetProperties().Where(p => p.GetCustomAttributes<JsonIgnoreAttribute>().FirstOrDefault() != null);
                    foreach (var ignoredProperty in ignoredProperties)
                    {
                        if (definition.Value.Properties.Any(x => x.Key.ToLower() == ignoredProperty.Name.ToLower()))
                            definition.Value.Properties.Remove(definition.Value.Properties.Where(x => x.Key.ToLower() == ignoredProperty.Name.ToLower()).First());
                    }
                }
            }
        }
    }
}
