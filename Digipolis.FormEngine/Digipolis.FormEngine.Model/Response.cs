﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digipolis.FormEngine.Model
{
    public class Response : IJsonContentHolder
    {
        public Guid Id { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public Guid TemplateLookupKey { get; set; }

        [Required]
        public string TemplateVersion { get; set; }

        [Required]
        [Column(TypeName = "timestamp with time zone")]
        public DateTime Creation { get; set; }
    }
}
