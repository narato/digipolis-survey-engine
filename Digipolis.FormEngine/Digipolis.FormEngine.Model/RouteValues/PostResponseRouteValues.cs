﻿using System;

namespace Digipolis.FormEngine.Model.RouteValues
{
    public class PostResponseRouteValues
    {
        public Guid Id { get; set; }
        public string TenantKey { get; set; }
        public string Controller { get; set; }
    }
}
