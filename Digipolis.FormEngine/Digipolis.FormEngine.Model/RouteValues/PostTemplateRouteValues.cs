﻿using System;

namespace Digipolis.FormEngine.Model.RouteValues
{
    public class PostTemplateRouteValues
    {
        public Guid TemplateLookupKey { get; set; }
        public string Version { get; set; }
        public string TenantKey { get; set; }
        public string Controller { get; set; }
    }
}
