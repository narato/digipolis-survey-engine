﻿namespace Digipolis.FormEngine.Model
{
    public class DatabaseConfig
    {
        public string DatabaseName { get; set; }
        public string Server { get; set; }
        public string Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
