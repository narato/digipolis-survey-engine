﻿using Newtonsoft.Json;
using System;

namespace Digipolis.FormEngine.Model.SwaggerResponseModels
{
    public class PutResponse : Response
    {
        [JsonIgnore]
        public new DateTime Creation { get; set; }

        public Response CastToParentType()
        {
            return new Response() { Id = this.Id, Content = this.Content, Creation = this.Creation, TemplateLookupKey = this.TemplateLookupKey, TemplateVersion = this.TemplateVersion };
        }
    }
}
