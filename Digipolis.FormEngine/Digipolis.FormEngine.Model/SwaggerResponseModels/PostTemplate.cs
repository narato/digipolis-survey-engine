﻿using Newtonsoft.Json;
using System;

namespace Digipolis.FormEngine.Model.SwaggerResponseModels
{
    public class PostTemplate : Template
    {
        [JsonIgnore]
        public new string Version { get; set; }
        [JsonIgnore]
        public new DateTime Creation { get; set; }
        [JsonIgnore]
        public new Guid LookupKey { get; set; }

        public Template CastToParentType()
        {
            return new Template { Id = this.Id, Content = this.Content, Creation = this.Creation, Version = this.Version, Description = this.Description, LookupKey = this.LookupKey, Name = this.Name, IsDraft = this.IsDraft };
        }
    }
}
