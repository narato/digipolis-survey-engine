﻿namespace Digipolis.FormEngine.Model
{
    public interface IJsonContentHolder
    {
        string Content { get; set; }
    }
}
