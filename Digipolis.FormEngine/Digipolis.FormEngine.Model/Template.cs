﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digipolis.FormEngine.Model
{
    public class Template : IJsonContentHolder
    {
        [JsonIgnore]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public Guid LookupKey { get; set; }
        public string Version { get; set; }
        public bool IsDraft { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [Column(TypeName = "timestamp with time zone")]
        public DateTime Creation { get; set; }
    }
}
