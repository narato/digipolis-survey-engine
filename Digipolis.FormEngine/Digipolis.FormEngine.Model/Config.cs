﻿
namespace Digipolis.FormEngine.Model
{
    // Acts as ConfigT in NuGet package
    public class Config
    {
        public DatabaseConfig ResponseDatabaseConfig { get; set; }
        public DatabaseConfig TemplateDatabaseConfig { get; set; }

        // for Event handler
        public string EventNamespace { get; set; }
        public string EventOwnerKey { get; set; }
    }
}
