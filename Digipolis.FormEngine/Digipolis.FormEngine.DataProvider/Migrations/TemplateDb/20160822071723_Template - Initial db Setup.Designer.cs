﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Digipolis.FormEngine.DataProvider.DbContexts;

namespace Digipolis.FormEngine.DataProvider.Migrations.TemplateDb
{
    [DbContext(typeof(TemplateDbContext))]
    [Migration("20160822071723_Template - Initial db Setup")]
    partial class TemplateInitialdbSetup
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rc2-20901");

            modelBuilder.Entity("Digipolis.FormEngine.Model.Template", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content")
                        .IsRequired();

                    b.Property<DateTime>("Creation")
                        .HasColumnType("timestamp with time zone");

                    b.Property<string>("Description");

                    b.Property<Guid>("LookupKey");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Version");

                    b.HasKey("Id");

                    b.ToTable("Template");
                });
        }
    }
}
