﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Digipolis.FormEngine.DataProvider.DbContexts;

namespace Digipolis.FormEngine.DataProvider.Migrations.TemplateDb
{
    [DbContext(typeof(TemplateDbContext))]
    [Migration("20161207104040_added_isdraft_to_templates")]
    partial class added_isdraft_to_templates
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("Digipolis.FormEngine.Model.Template", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content")
                        .IsRequired();

                    b.Property<DateTime>("Creation")
                        .HasColumnType("timestamp with time zone");

                    b.Property<string>("Description");

                    b.Property<bool>("IsDraft");

                    b.Property<Guid>("LookupKey");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Version");

                    b.HasKey("Id");

                    b.ToTable("Template");
                });
        }
    }
}
