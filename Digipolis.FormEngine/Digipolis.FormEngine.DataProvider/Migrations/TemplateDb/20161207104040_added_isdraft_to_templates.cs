﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Digipolis.FormEngine.DataProvider.Migrations.TemplateDb
{
    public partial class added_isdraft_to_templates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.EnsurePostgresExtension("uuid-ossp");

            migrationBuilder.AddColumn<bool>(
                name: "IsDraft",
                table: "Template",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropPostgresExtension("uuid-ossp");

            migrationBuilder.DropColumn(
                name: "IsDraft",
                table: "Template");
        }
    }
}
