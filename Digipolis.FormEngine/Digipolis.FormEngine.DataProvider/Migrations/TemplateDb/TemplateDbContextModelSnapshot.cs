﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Digipolis.FormEngine.DataProvider.DbContexts;

namespace Digipolis.FormEngine.DataProvider.Migrations.TemplateDb
{
    [DbContext(typeof(TemplateDbContext))]
    partial class TemplateDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("Digipolis.FormEngine.Model.Template", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content")
                        .IsRequired();

                    b.Property<DateTime>("Creation")
                        .HasColumnType("timestamp with time zone");

                    b.Property<string>("Description");

                    b.Property<bool>("IsDraft");

                    b.Property<Guid>("LookupKey");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Version");

                    b.HasKey("Id");

                    b.ToTable("Template");
                });
        }
    }
}
