﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Digipolis.FormEngine.DataProvider.DbContexts;

namespace Digipolis.FormEngine.DataProvider.Migrations
{
    [DbContext(typeof(ResponseDbContext))]
    partial class ResponseDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rc2-20901");

            modelBuilder.Entity("Digipolis.FormEngine.Model.Response", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content")
                        .IsRequired();

                    b.Property<DateTime>("Creation")
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid>("TemplateLookupKey");

                    b.Property<string>("TemplateVersion")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Response");
                });
        }
    }
}
