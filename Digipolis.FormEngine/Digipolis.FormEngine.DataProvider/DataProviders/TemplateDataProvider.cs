﻿using Digipolis.Common.Status.Models;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Model;
using Microsoft.EntityFrameworkCore;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.DataProvider.DataProviders
{
    public class TemplateDataProvider : ITemplateDataProvider
    {
        private readonly ITemplateDbContextProvider _templateDbContextProvider;

        public TemplateDataProvider(ITemplateDbContextProvider templateDbContextProvider)
        {
            _templateDbContextProvider = templateDbContextProvider;
        }

        public async Task<Template> GetTemplateByLookupKeyAndVersionAsync(Guid lookupKey, string version)
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                return await context.Template.FirstOrDefaultAsync(x => x.LookupKey == lookupKey && x.Version == version);
            }
        }

        public async Task<Template> GetLastInsertedTemplateScopedAsync(Guid lookupKey, TemplateScope scope)
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                return await context.Template
                .OrderByDescending(t => t.Creation)
                .FirstOrDefaultAsync(t => t.LookupKey == lookupKey
                    && ((scope == TemplateScope.PUBLISHED ? !t.IsDraft : t.IsDraft)
                    || (scope == TemplateScope.DRAFT ? t.IsDraft : !t.IsDraft)));
            }
        }

        public async Task<Template> InsertTemplateAsync(Template template)
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                context.Template.Add(template);
                await context.SaveChangesAsync();
            }

            return template;
        }

        public async Task<Template> UpdateTemplateAsync(Template template)
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                context.Template.Attach(template);
                context.Entry(template).State = EntityState.Modified;
                await context.SaveChangesAsync();
            }

            return template;
        }

        public async Task<Template> DeleteTemplateVersionAsync(Guid lookupKey, string version)
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                var original = await context.Template.FirstOrDefaultAsync(x => x.LookupKey == lookupKey && x.Version == version);

                if (original != null)
                {
                    context.Entry(original).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                else
                    throw new ValidationException(new FeedbackItem { Description = $"Template with lookupkey '{lookupKey} and version '{version}' was not found. Delete cannot continue.'", Type = FeedbackType.ValidationError });

                return original;
            }
        }

        public async Task<DbConnectionCheckResult> CheckDBConnectionAsync()
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                try
                {
                    await context.Database.OpenConnectionAsync();
                    await context.Database.ExecuteSqlCommandAsync("SELECT N'Test'");
                    context.Database.CloseConnection();
                }
                catch (Exception ex)
                {
                    return new DbConnectionCheckResult(false, ex);
                }

                return new DbConnectionCheckResult(true);
            }
        }

        public async Task<IEnumerable<Model.Template>> GetTemplatesAsync()
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                return await context.Template.ToListAsync();
            }
        }

        public async Task<IEnumerable<Template>> GetTemplatesAsync(int page, int pageSize)
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                return await context.Template.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
            }
        }

        public async Task<IEnumerable<Template>> GetTemplatesByLookupKeyScopedAsync(Guid lookupKey, TemplateScope scope, int page, int pageSize)
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                return await context.Template
                    .OrderByDescending(t => t.Creation)
                    .Where(t => t.LookupKey == lookupKey
                        && ((scope == TemplateScope.PUBLISHED ? !t.IsDraft : t.IsDraft)
                        || (scope == TemplateScope.DRAFT ? t.IsDraft : !t.IsDraft)))
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync();
            }
        }

        public async Task<int> GetCountByLookupKeyScopedAsync(Guid lookupKey, TemplateScope scope)
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                return await context.Template
                    .Where(t => t.LookupKey == lookupKey
                        && ((scope == TemplateScope.PUBLISHED ? !t.IsDraft : t.IsDraft)
                        || (scope == TemplateScope.DRAFT ? t.IsDraft : !t.IsDraft)))
                    .CountAsync();
            }
        }

        public async Task<int> GetCountAsync()
        {
            using (var context = await _templateDbContextProvider.ProvideAsync())
            {
                return await context.Template.CountAsync();
            }
        }
    }
}
