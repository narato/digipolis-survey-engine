﻿using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.DataProvider.DataProviders;
using System.Threading.Tasks;
using Digipolis.FormEngine.DataProvider.DbContexts;

namespace Digipolis.FormEngine.Domain.Providers
{
    public class ResponseDbContextProvider : IResponseDbContextProvider
    {
        private readonly ICurrentTenantProvider<Config> _currentTenantProvider;

        public ResponseDbContextProvider(ICurrentTenantProvider<Config> currentTenantProvider)
        {
            _currentTenantProvider = currentTenantProvider;
        }

        public async Task<IResponseDbContext> ProvideAsync()
        {
            var context = new ResponseDbContext();
            var tenant = await _currentTenantProvider.GetCurrentTenantAsync();
            context.DatabaseConfig = tenant.Config.ResponseDatabaseConfig;

            return context;
        }
    }
}
