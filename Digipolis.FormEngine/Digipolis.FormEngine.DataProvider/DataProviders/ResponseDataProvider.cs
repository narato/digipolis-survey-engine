﻿using Digipolis.Common.Status.Models;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Microsoft.EntityFrameworkCore;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.DataProvider.DataProviders
{
    public class ResponseDataProvider : IResponseDataProvider
    {
        private readonly IResponseDbContextProvider _responseDbContextProvider;

        public ResponseDataProvider(IResponseDbContextProvider responseDbContextProvider)
        {
            _responseDbContextProvider = responseDbContextProvider;
        }

        public async Task<Model.Response> GetResponseByIdAsync(Guid id)
        {
            using (var context = await _responseDbContextProvider.ProvideAsync())
            {
               return await context.Response.FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async Task<Model.Response> InsertResponseAsync(Model.Response response)
        {
            using (var context = await _responseDbContextProvider.ProvideAsync())
            {
                context.Response.Add(response);
                await context.SaveChangesAsync();
            }
            
            return response;
        }

        public async Task<Model.Response> UpdateResponseAsync(Model.Response response)
        {
            using (var context = await _responseDbContextProvider.ProvideAsync())
            {
                var original = await context.Response.FirstOrDefaultAsync(x => x.Id == response.Id);

                if (original != null)
                {
                    original.Content = response.Content;
                    original.TemplateLookupKey = response.TemplateLookupKey;
                    original.TemplateVersion = response.TemplateVersion;

                    await context.SaveChangesAsync();
                }
                else
                    throw new ValidationException(new FeedbackItem { Description = $"Response with id '{response.Id} was not found. Update cannot continue.'", Type = FeedbackType.ValidationError });

                return original;
            }
        }

        public async Task<DbConnectionCheckResult> CheckDBConnectionAsync()
        {
            using (var context = await _responseDbContextProvider.ProvideAsync())
            {
                try
                {
                    await context.Database.OpenConnectionAsync();
                    await context.Database.ExecuteSqlCommandAsync("SELECT N'Test'");
                    context.Database.CloseConnection();
                }
                catch (Exception ex)
                {
                    return new DbConnectionCheckResult(false, ex);
                }

                return new DbConnectionCheckResult(true);
            }
        }

        public async Task<IEnumerable<Model.Response>> GetResponseByTemplateAsync(Guid templateLookupKey, string templateVersion, int page, int pageSize)
        {
            using (var context = await _responseDbContextProvider.ProvideAsync())
            {
                return await context.Response.Where(x => x.TemplateLookupKey == templateLookupKey && x.TemplateVersion == templateVersion).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
            }
        }

        public async Task<Model.Response> DeleteResponseAsync(Guid id)
        {
            using (var context = await _responseDbContextProvider.ProvideAsync())
            {
                var original = await context.Response.FirstOrDefaultAsync(x => x.Id == id);

                if (original != null)
                {
                    context.Entry(original).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                else
                    throw new ValidationException(new FeedbackItem { Description = $"Response with id '{id} was not found. Delete cannot continue.'", Type = FeedbackType.ValidationError });

                return original;
            }
        }

        public async Task<int> GetCountByTemplateAsync(Guid templateLookupKey, string templateVersion)
        {
            using (var context = await _responseDbContextProvider.ProvideAsync())
            {
                return await context.Response.Where(x => x.TemplateLookupKey == templateLookupKey && x.TemplateVersion == templateVersion).CountAsync();
            }
        }
    }
}
