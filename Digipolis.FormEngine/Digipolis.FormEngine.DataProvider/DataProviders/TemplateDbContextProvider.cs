﻿using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Model;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.DataProvider.DataProviders;
using System.Threading.Tasks;
using Digipolis.FormEngine.DataProvider.DbContexts;

namespace Digipolis.FormEngine.Domain.Providers
{
    public class TemplateDbContextProvider : ITemplateDbContextProvider
    {
        private readonly ICurrentTenantProvider<Config> _currentTenantProvider;

        public TemplateDbContextProvider(ICurrentTenantProvider<Config> currentTenantProvider)
        {
            _currentTenantProvider = currentTenantProvider;
        }

        public async Task<ITemplateDbContext> ProvideAsync()
        {
            var context = new TemplateDbContext();
            var tenant = await _currentTenantProvider.GetCurrentTenantAsync();
            context.DatabaseConfig = tenant.Config.TemplateDatabaseConfig;

            return context;
        }
    }
}
