﻿using Digipolis.FormEngine.DataProvider.Interfaces;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.DataProvider.DataProviders
{
    public interface IResponseDbContextProvider
    {
        Task<IResponseDbContext> ProvideAsync();
    }
}
