﻿using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Model;
using Microsoft.EntityFrameworkCore;

namespace Digipolis.FormEngine.DataProvider.DbContexts
{
    public class ResponseDbContext : BaseDbContext, IResponseDbContext
    {
        public DbSet<Response> Response { get; set; }

        public ResponseDbContext() { }

        public ResponseDbContext(string connectionString):base(connectionString){}

        public ResponseDbContext(DbContextOptions options) : base(options)
        {
            _options = options;
        }
    }
}

