﻿using Digipolis.FormEngine.Model;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL;

namespace Digipolis.FormEngine.DataProvider.DbContexts
{
    public abstract class BaseDbContext : DbContext
    {
        public DatabaseConfig DatabaseConfig { get; set; }
        public DbContextOptions _options;

        protected string _connectionString;

        public BaseDbContext() { }

        public BaseDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public BaseDbContext(DbContextOptions options) : base(options)
        {
            _options = options;
        }

        public void SetConnectionString(DbContextOptionsBuilder optionsBuilder)
        {
            if (_options == null)
            {
                //_connectionString = $"Server = 192.168.99.100; Port = 5431; Database=migrationsDatabaseResponse; User Id = postgres; Password = narato";
                _connectionString = $"Server = {DatabaseConfig.Server}; Port = {DatabaseConfig.Port}; Database={DatabaseConfig.DatabaseName}; User Id = {DatabaseConfig.User}; Password = {DatabaseConfig.Password}";
                optionsBuilder.UseNpgsql(_connectionString);
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            SetConnectionString(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasPostgresExtension("uuid-ossp");
        }
    }
}
