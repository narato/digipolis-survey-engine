﻿using Digipolis.FormEngine.DataProvider.Interfaces;
using Digipolis.FormEngine.Model;
using Microsoft.EntityFrameworkCore;

namespace Digipolis.FormEngine.DataProvider.DbContexts
{
    public class TemplateDbContext : BaseDbContext, ITemplateDbContext
    {
        public DbSet<Template> Template { get; set; }

        public TemplateDbContext() { }

        public TemplateDbContext(string connectionString):base(connectionString){}

        public TemplateDbContext(DbContextOptions options) : base(options)
        {
            _options = options;
        }
    }
}
