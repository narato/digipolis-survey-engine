﻿using Digipolis.Common.Status.Models;
using Digipolis.FormEngine.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.DataProvider.Interfaces
{
    public interface IResponseDataProvider
    {
        Task<Model.Response> GetResponseByIdAsync(Guid id);
        Task<IEnumerable<Model.Response>> GetResponseByTemplateAsync(Guid templateLookupKey, string templateVersion, int page, int pageSize);
        Task<Response> InsertResponseAsync(Model.Response response);
        Task<Model.Response> UpdateResponseAsync(Model.Response response);
        Task<DbConnectionCheckResult> CheckDBConnectionAsync();
        Task<Model.Response> DeleteResponseAsync(Guid id);
        Task<int> GetCountByTemplateAsync(Guid templateLookupKey, string templateVersion);
    }
}
