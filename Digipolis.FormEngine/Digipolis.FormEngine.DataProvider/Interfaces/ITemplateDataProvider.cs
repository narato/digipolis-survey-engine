﻿using Digipolis.Common.Status.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.DataProvider.Interfaces
{
    public enum TemplateScope
    {
        PUBLISHED,
        DRAFT,
        EITHER
    }

    public interface ITemplateDataProvider
    {
        Task<Model.Template> GetTemplateByLookupKeyAndVersionAsync(Guid lookupKey, string version);
        Task<Model.Template> GetLastInsertedTemplateScopedAsync(Guid lookupKey, TemplateScope scope);
        Task<IEnumerable<Model.Template>> GetTemplatesAsync();
        Task<IEnumerable<Model.Template>> GetTemplatesAsync(int page, int pageSize);
        Task<IEnumerable<Model.Template>> GetTemplatesByLookupKeyScopedAsync(Guid lookupKey, TemplateScope scope, int page, int pageSize);
        Task<int> GetCountByLookupKeyScopedAsync(Guid lookupKey, TemplateScope scope);
        Task<Model.Template> InsertTemplateAsync(Model.Template template);
        Task<Model.Template> UpdateTemplateAsync(Model.Template template);
        Task<Model.Template> DeleteTemplateVersionAsync(Guid lookupKey, string version);
        Task<DbConnectionCheckResult> CheckDBConnectionAsync();
        Task<int> GetCountAsync();
    }
}
