﻿using Digipolis.FormEngine.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.DataProvider.Interfaces
{
    public interface IResponseDbContext : IDisposable
    {
        DatabaseConfig DatabaseConfig { get; set; }
        DbSet<Response> Response { get; set; }
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        DatabaseFacade Database { get; }
        EntityEntry Entry(object entity);
    }
}
