﻿namespace Digipolis.FormEngine.DataProvider.Interfaces
{
    public interface ISystemStatusProvider
    {
        bool CheckDBConnection();
    }
}
