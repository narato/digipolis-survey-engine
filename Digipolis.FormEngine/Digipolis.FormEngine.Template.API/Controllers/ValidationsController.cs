﻿using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Model.SwaggerResponseModels;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Digipolis.FormEngine.Template.API.Controllers
{
    [Route("api/[controller]")]
    public class ValidationsController : Controller
    {
        private ITemplateManager _templateManager;
        private IResponseFactory _responseFactory;

        public ValidationsController(ITemplateManager templateManager, IResponseFactory responseFactory)
        {
            _templateManager = templateManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// This method validates a template against the json schema
        /// </summary>
        /// <param name="template">The template to be validated</param>
        /// <param name="tenantKey">The id of the tenant </param>
        /// <returns>The template that was saved</returns>
        // POST api/Response
        [ProducesResponseType(typeof(Narato.Common.Models.Response), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Narato.Common.Models.Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Narato.Common.Models.Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpPost]
        public IActionResult Post([FromBody][Required]PostTemplate template, [FromHeader]string tenantKey)
        {
            // TODO: refactor this to use a Validator (that gets DI'ed), Also has to be within the lambda function of the ResponseFactory, else it doesn't fall within the try catch it provides
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.Header));
            if (template == null)
                missingParams.Add(new MissingParam("template", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);
            // END TODO

            return _responseFactory.CreateGetResponse(() => _templateManager.ValidateTemplate(template), this.GetRequestUri());
        }
    }
}
