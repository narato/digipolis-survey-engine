﻿using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.Model.RouteValues;
using Digipolis.FormEngine.Model.SwaggerResponseModels;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.FormEngine.Template.API.Controllers
{
    [Route("api/[controller]")]
    public class TemplatesController : Controller
    {
        private ITemplateManager _templateManager;
        private IResponseFactory _responseFactory;

        public TemplatesController(ITemplateManager templateManager, IResponseFactory responseFactory)
        {
            _templateManager = templateManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// This method returns 1 template identified by its id for the given tenantid
        /// </summary>
        /// <param name="templateLookupKey">The lookupKey of the template to retrieve</param>c
        /// <param name="version">The version of the template to retrieve</param>
        /// <param name="tenantKey">The id of the tenant </param>
        /// <returns>The template identified by the id for the given tenantid</returns>
        // GET api/Response
        [ProducesResponseType(typeof(Response<Model.Template>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("{templateLookupKey}/versions/{version}", Name = "GetTemplateByLookupKeyAndVersion")]
        public async Task<IActionResult> GetAsync([Required]Guid templateLookupKey, [Required] string version, [FromHeader]string tenantKey)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));

            return await _responseFactory.CreateGetResponseAsync(async () => await _templateManager.GetTemplateByLookupKeyAndVersionAsync(templateLookupKey, version), this.GetRequestUri());
        }

        /// <summary>
        /// This method returns all templates identified by its lookupkey for the given tenantid defined by a scope
        /// </summary>
        /// <param name="templateLookupKey">The lookupKey of the template to retrieve</param>c
        /// <param name="scope">The scope of the templates (draft, published and latest)</param>
        /// <param name="tenantKey">The id of the tenant </param>
        /// <param name="page">The page for the pagination </param>
        /// <param name="pagesize">The page size for the pagination</param>
        /// <returns>The template identified by the id for the given tenantid</returns>
        // GET api/Response
        [ProducesResponseType(typeof(Response<Model.Template>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet("{templateLookupKey}/versions", Name = "GetTemplateVersionsByLookupKey")]
        public async Task<IActionResult> GetVersionsForScopeAsync([Required]Guid templateLookupKey, [FromQuery] string[] scope, [FromHeader]string tenantKey, [FromQuery] int page = 1, [FromQuery] int pagesize = 10)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));

            return await _responseFactory.CreateGetResponseForCollectionAsync(async () => await _templateManager.GetTemplateByLookupKeyAndScopeAsync(templateLookupKey, scope, page, pagesize), this.GetRequestUri());
        }

        /// <summary>
        /// This method returns all template for the given tenantid
        /// </summary>
        /// <param name="tenantKey">The id of the tenant </param>
        /// <param name="page">The page for the pagination </param>
        /// <param name="pagesize">The page size for the pagination</param>
        /// <returns>The templates for the given tenantid</returns>
        // GET api/Response
        [ProducesResponseType(typeof(Response<Model.Template>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpGet()]
        public async Task<IActionResult> GetTemplateForTenantAsync([FromHeader]string tenantKey, [FromQuery] int page = 1, [FromQuery] int pagesize = 10)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));

            return await _responseFactory.CreateGetResponseForCollectionAsync(async () => await _templateManager.GetTemplatesForTenantAsync(page, pagesize), this.GetRequestUri());
        }

        /// <summary>
        /// This method saves 1 template given in the body of the request
        /// </summary>
        /// <param name="template">The template to be saved</param>
        /// <param name="tenantKey">The id of the tenant </param>
        /// <returns>The template that was saved</returns>
        // POST api/Response
        [ProducesResponseType(typeof(Response<Model.Template>), (int)System.Net.HttpStatusCode.Created)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody][Required]PostTemplate template, [FromHeader]string tenantKey)
        {
            // TODO: refactor this to use a Validator (that gets DI'ed), Also has to be within the lambda function of the ResponseFactory, else it doesn't fall within the try catch it provides
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.Header));
            if (template == null)
                missingParams.Add(new MissingParam("template", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);
            // END TODO

            return await _responseFactory.CreatePostResponseAsync(async () => await _templateManager.InsertTemplateAsync(template.CastToParentType()), this.GetRequestUri(), "GetTemplateByLookupKeyAndVersion", new PostTemplateRouteValues { Controller = "Template", TemplateLookupKey = template.LookupKey, Version = template.Version }, new List<RouteValuesIdentifierPair> { new RouteValuesIdentifierPair { ModelIdentifier = "LookupKey", RouteValuesIdentifier = "TemplateLookupKey" }, new RouteValuesIdentifierPair { ModelIdentifier = "Version", RouteValuesIdentifier = "Version" } });
        }

        /// <summary>
        /// This method saves 1 template given in the body of the request
        /// </summary>
        /// <param name="template">The template to be saved</param>
        /// <param name="tenantKey">The id of the tenant </param>
        /// <returns>The template that was saved</returns>
        // POST api/Response
        [ProducesResponseType(typeof(Response<Model.Template>), (int)System.Net.HttpStatusCode.Created)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody][Required]PutTemplate template, [FromHeader]string tenantKey)
        {
            // TODO: refactor this to use a Validator (that gets DI'ed), Also has to be within the lambda function of the ResponseFactory, else it doesn't fall within the try catch it provides
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.Header));
            if (template == null)
                missingParams.Add(new MissingParam("template", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);
            // END TODO

            return await _responseFactory.CreatePostResponseAsync(async () => await _templateManager.UpdateTemplateAsync(template.CastToParentType()), this.GetRequestUri(), "GetTemplateByLookupKeyAndVersion", new PostTemplateRouteValues { Controller = "Template", TemplateLookupKey = template.LookupKey, Version = template.Version }, new List<RouteValuesIdentifierPair> { new RouteValuesIdentifierPair { ModelIdentifier = "LookupKey", RouteValuesIdentifier = "TemplateLookupKey" }, new RouteValuesIdentifierPair { ModelIdentifier = "Version", RouteValuesIdentifier = "Version" } });
        }

        /// <summary>
        /// This method deletes 1 template given in the body of the request
        /// </summary>
        /// <param name="templateLookupKey">The template lookupkey of the template to be deleted</param>
        /// <param name="version">The template version of the template to be deleted</param>
        /// <param name="tenantKey">The id of the tenant </param>
        /// <returns>The template that was saved</returns>
        // POST api/Response
        [ProducesResponseType(typeof(Response<Model.Template>), (int)System.Net.HttpStatusCode.Accepted)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response<object>), (int)System.Net.HttpStatusCode.InternalServerError)]
        [HttpDelete("{templateLookupKey}/versions/{version}")]
        public async Task<IActionResult> DeleteAsync([Required]Guid templateLookupKey, [Required] string version, [Required][FromHeader]string tenantKey)
        {
            // TODO: refactor this to use a Validator (that gets DI'ed), Also has to be within the lambda function of the ResponseFactory, else it doesn't fall within the try catch it provides
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.Header));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);
            // END TODO

            return await _responseFactory.CreateGetResponseAsync(async () => await _templateManager.DeleteTemplateAsync(templateLookupKey, version), this.GetRequestUri());
        }
    }
}