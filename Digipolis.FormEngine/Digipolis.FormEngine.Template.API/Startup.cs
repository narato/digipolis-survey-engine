﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Reflection;
using Autofac;
using Digipolis.FormEngine.Domain.Managers;
using Digipolis.FormEngine.Domain.Interfaces;
using Digipolis.FormEngine.DataProvider.DataProviders;
using Digipolis.FormEngine.DataProvider.Interfaces;
using Autofac.Extensions.DependencyInjection;
using Digipolis.FormEngine.Domain.Clients;
using Digipolis.FormEngine.Common.Models;
using Digipolis.FormEngine.Model.Validator;
using Digipolis.FormEngine.Model.Validator.Interfaces;
using Narato.Common.ActionFilters;
using Narato.Common.Exceptions;
using Narato.Common.Interfaces;
using Narato.Common.Factory;
using Digipolis.Common.Status.Interfaces;
using Digipolis.FormEngine.Domain.Providers;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.FormEngine.Model;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using Digipolis.FormEngine.Common;
using Digipolis.FormEngine.Domain.AutomaticMigration;
using Swashbuckle.Swagger.Model;
using NLog.Extensions.Logging;
using Digipolis.FormEngine.Model.SwaggerSchemaFilter;
using Narato.Common.Checksum;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json;
using Digipolis.Common.Status.Models;
using Narato.Common.Mappers;
using Digipolis.FormEngine.Domain.Converters;

namespace Digipolis.FormEngine.Template.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("config.json");

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.Configure<EngineConfiguration>(Configuration.GetSection("EngineConfiguration"));
            services.Configure<SystemConfiguration>(Configuration.GetSection("SystemConfiguration"));

            services.AddCors();

            services.AddMvc(
            //Add this filter globally so every request runs this filter to recored execution time
            config =>
            {
                config.Filters.Add(new ExecutionTimingFilter());
            })
                //Add formatter for JSON output to client and to format received objects         
                .AddJsonOptions(x =>
                {
                    x.SerializerSettings.ContractResolver =
                     new CamelCasePropertyNamesContractResolver();
                    x.SerializerSettings.Converters.Add(new JsonContentHolderConverter());
                });

            var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            //Swagger configuation + swagger UI
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Contact = new Contact { Name = "Narato NV" },
                    Description = "Template API",
                    Version = "v1",
                    Title = "Form & Survey Engine"
                });
                options.IncludeXmlComments($"{path}/Digipolis.FormEngine.Template.API.xml");
                options.OperationFilter<ProducesConsumesFilter>();
                options.DocumentFilter<OmitIgnoredPropertiesDocumentfilter>();
                options.DocumentFilter<ChangeTypeOfJsonContentFilter>();
            });

            //Gets the Admin Api Endpoint from the config.json file
            var engineConfigSection = Configuration.GetSection("EngineConfiguration");
            var adminApiEndpoint = engineConfigSection.GetValue<string>("AdminApiEndpoint");
            var responseApiEndpoint = engineConfigSection.GetValue<string>("ResponseApiEndpoint");

            //Confgure Autofac
            var builder = new ContainerBuilder();

            builder.RegisterType<ExceptionHandler>().As<IExceptionHandler>();
            builder.RegisterType<ExceptionToActionResultMapper>().As<IExceptionToActionResultMapper>();
            builder.RegisterType<TenantClient>().As<ITenantClient>().WithParameter("httpClient", new HttpClient() { BaseAddress = new Uri(adminApiEndpoint) });
            builder.RegisterType<CurrentTenantProvider>().As<ICurrentTenantProvider<Config>>();
            builder.RegisterType<ResponseFactory>().As<IResponseFactory>();
            builder.RegisterType<TemplateManager>().As<ITemplateManager>();
            builder.RegisterType<SystemStatusTemplateManager>().As<ISystemStatusManager>();
            builder.RegisterType<TemplateDbContextProvider>().As<ITemplateDbContextProvider>();
            builder.RegisterType<TemplateDataProvider>().As<ITemplateDataProvider>();
            builder.RegisterType<ResponseClient>().As<IResponseClient>().WithParameter("httpClient", new System.Net.Http.HttpClient() { BaseAddress = new Uri(responseApiEndpoint) });
            builder.RegisterType<TemplateValidator>().As<ITemplateValidator>();

            builder.RegisterType<ChecksumCalculator>().AsSelf();

            using (StreamReader file = File.OpenText(Directory.GetCurrentDirectory() + @"/templateSchema.json"))
            {
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    JSchema schema = JSchema.Load(reader);
                    builder.RegisterInstance(schema);
                }
            }

            builder.Populate(services);
            var container = builder.Build();

            // Return the IServiceProvider resolved from the container.
            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            
            loggerFactory.AddNLog();

            env.ConfigureNLog("nlog.config");

            app.UseApplicationInsightsRequestTelemetry();

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            app.UseMvc();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUi();

            var engineConfigSection = Configuration.GetSection("EngineConfiguration");
            var adminApiEndpoint = engineConfigSection.GetValue<string>("AdminApiEndpoint");

            new TemplateAutoMigration(new TenantClient(new HttpClient() { BaseAddress = new Uri(adminApiEndpoint) })).Migrate();
        }
    }
}
